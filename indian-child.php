<?php
	include_once('header.php');
?>

        <!-- start tab style 04 section -->
        <section class="wow fadeIn padding-six-tb bg-light-gray" style="margin-top: 123px;">
            <div class="container tab-style4">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12 margin-30px-bottom xs-margin-40px-bottom">
                        <div class="position-relative overflow-hidden width-100">
                            <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Indian Child Welfare

                            </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding-right" style="border-right: 1px solid #e5e5e5;">
                        <div class="display-table width-100 height-100">
                            <div class="display-table-cell vertical-align-middle">
                                <!-- start tab navigation -->
                                <ul class="nav nav-tabs alt-font text-uppercase text-small display-inherit font-weight-600">
                                    <li class="active"><a href="#tab-four1" data-toggle="tab">Indian Child Welfare

                                    </a></li>
                                    <li><a href="#tab-four2" data-toggle="tab">Angel Project</a></li>
                                    <li><a href="#tab-four3" data-toggle="tab">Indian Child Welfare Act</a></li>
                                    <li><a href="#tab-four4" data-toggle="tab">Become a Foster / Adoptive Parent</a></li>
                                    <li><a href="#tab-four5" data-toggle="tab">Parent Resources 101</a></li>
                                    <li><a href="#tab-four6" data-toggle="tab">Guardianships and Adoptions</a></li>
                                    <li><a href="#tab-four7" data-toggle="tab">Child Protective Services</a></li>
                                    <li><a href="#tab-four8" data-toggle="tab">Court Advocacy and Permanency Services</a></li>
                                    <li><a href="#tab-four9" data-toggle="tab">Frequently Asked Questions</a></li>
                                    <li><a href="#tab-four10" data-toggle="tab">Contact Us</a></li>
                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 no-padding-left">
                        <div class="tab-content" style="border: 0">
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in active" id="tab-four1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Indian Child Welfare provides a variety of protective and support services to Cherokee children and their families. 
                                                <br><br>
                                                Our Mission
                                                <br><br>
                                                Remembering our past, protecting the present and securing our future for generations to come by providing an array of protective, supportive, and child focused services to families needing assistance in maintaining a self-sufficient and healthy environment.
                                                <br><br>
                                                Who are the children?
                                                <br><br>
                                                > Native American children in custody of a tribe or state.
                                                <br>
                                                > Native American children ages 0-17.
                                                <br>
                                                > Native American children in sibling groups.
                                                <br>
                                                > Native American children with special physical or emotional needs.
                                                <br>
                                                > Native American children mixed with another ethnicity.  
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four2">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;">Angel Project
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Concurrent Enrollment Scholarships
                                                <br><br>
                                                High school students who are concurrently enrolled and receiving college credit may apply for the concurrent scholarship offered by the Cherokee Nation College Resource Center.
                                                <br><br>
                                                Concurrent education scholarships are offered to citizens of the Cherokee Nation who are high school juniors or seniors and are taking required general education courses, at a college or university, while still in high school. Tribal citizens must also have permanent residence within the Cherokee Nation or contiguous areas. Residency restrictions apply, see the complete map under the “Downloads” section. No private or proprietary institutions qualify.
                                                <br><br>
                                                Valedictorian/Salutatorian Scholarships
                                                <br><br>
                                                The Cherokee Nation recognizes and awards scholarship funds to Cherokee Nation citizens who are named Valedictorian/Salutatorian for their graduating high school class. High schools located within the Cherokee Nation scholarship service area and contiguous counties are eligible.
                                                <br><br>
                                                Higher Education Scholarships, Undergraduate and Graduate
                                                <br><br>
                                                Citizens of the Cherokee Nation actively pursuing a higher education degree may apply to receive educational scholarships payable directly to an accredited college or university. Some scholarships may have restrictions, such as students with multiple tribal lineages may only receive assistance from one tribe.  Students must be working towards an Associates in Arts or Science, Associates of Applied Science in Nursing ONLY, Bachelors, Masters, or Doctorate degrees. Students may receive assistance with one Associates Degree, one Bachelor’s Degree, one Master’s Degree, and one Doctorate Degree.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four3">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Indian Child Welfare Act
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The ᏣᎳᎩ Cultural Resource Center is a Cherokee Nation program with the goal to educate both tribal citizens and the general public on Cherokee language, history, and lifeways. It was created by the Cherokee Nation Tribal Council in 1991.
                                                <br><br>
                                                The Cherokee Cultural Resource Center provides outreach to schools and communities through cultural presentations, special events and more.
                                                <br><br>
                                                Special Projects
                                                <br><br>
                                                Public school superintendents’ T.O.S.S. (Techniques of Successful Superintendents) <br>
                                                T.O.S.S. (Teachers of Successful Students) Institute <br>
                                                Provides technical assistance to Trail of Tears Awards for Excellence, Cherokee Nation Tribal Youth Council, the Remember the Removal Bike Ride, Archery and Ambassador Programs including Miss Cherokee, Junior Miss Cherokee and Little Cherokee Ambassadors <br>
                                           </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four4">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Become a Foster / Adoptive Parent
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Mission Statement:
                                                <br><br>
                                                The mission of the Early Childhood Unit is to bring about a greater degree of social competence in children who may be disadvantaged.
                                                <br><br>
                                                The ECU’s approach is based on the philosophy that a child can benefit most from a comprehensive, interdisciplinary program to foster development and remedy problems through the involvement of the child’s entire family as well as the community.
                                                <br><br>
                                                Programs:
                                                <br><br>
                                                Cherokee Nation Early Childhood Unit offers two options: Head Start, a classroom environment promoting early childhood development stages; and Early Head Start, serving children aged 6 weeks to three years old.
                                                <br><br>
                                                Head Start serves children age three to five at centers in these areas: Brushy, Hulbert, Inola, Kenwood, Stilwell, Pryor, Rocky Mountain, Salina, Shady Grove, Tahlequah, Webbers Falls, and Zion.
                                                <br><br>
                                                Early Head Start serves children age six weeks to three years at centers in these areas: Tahlequah, Cherry Tree, Jay, Pryor, Stilwell, Salina, and Nowata.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four5">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Parent Resources 101
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The mission of the Cherokee Nation Johnson O’Malley (JOM) program is to provide opportunities to Native children for achievement through student and parent involvement in academic and cultural education.
                                                <br><br>
                                                The Johnson-O'Malley Program is a federally funded program authorized by the Johnson-O'Malley Act of 1934. This Act authorizes contracts for the education of eligible Indian students enrolled in public schools and previously private schools. JOM is a local program is operated under an educational plan, approved by the Bureau of Indian Education, and contains educational objectives to address the needs of the eligible American Indian and Alaska Native students. Johnson-O’Malley funds under this program may not be used for capital expenditures or individual expenditures such as clothing, computers or other personal items.
                                                <br><br>
                                                Johnson O’Malley requires eligible American Indian and Alaska Native students to be at least one of the following:
                                                <br><br>
                                                An enrolled citizen of a federally recognized tribe
                                                <br><br>
                                                OR
                                                <br><br>
                                                A one-fourth or higher degree of Indian blood descendant of a citizen of a federally recognized Indian tribal government and eligible for services from the Bureau of Indian Affairs.
                                                <br><br>
                                                In addition, eligibility requires the children to be between age 3 through grade 12, with priority given to children residing on or near an Indian reservation.
                                                <br><br>
                                                Proof of tribal citizenship and enrollment in a Cherokee Nation JOM program are the only requirements for Cherokee Nation JOM services.
                                                <br><br>
                                                The JOM program offers multiple opportunities for JOM students in grades K-12 to exercise their use of the Cherokee language, increase their knowledge of Cherokee history and tribal government, and expand their cultural knowledge through student competitions in art, writing, the Cherokee Challenge Bowl and the Cherokee Language Bowl.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four6">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Guardianships and Adoptions
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Cherokee Nation’s Education Outreach services provide opportunities to area public schools and communities.
                                                <br><br>
                                                Outreach Services includes cultural art, archery, robotics, STEM, culture, traditional games, and planetarium classrooms.
                                                <br><br>
                                                Archery
                                                <br><br>
                                                The Joe Thornton Archery Park is a free community park, devoted to all forms of outdoor archery as recreation and pastime for the whole family. The park is designed to be used by ages 8 and up, and for all skill levels from beginner to the advanced archer.
                                                <br><br>
                                                The archery park includes the following components:
                                                <br><br>
                                                > Certified archery coaches <br>
                                                > A general beginner area that includes a target practice area with marked distances from 15—50 yards <br>
                                                > A bow hunting simulation area that includes a platform <br>
                                                > Three-dimensional walking course to simulate natural hunting conditions 

                                                <br><br>

                                                STEM
                                                <br><br>
                                                The STEM Program’s focus includes science, technology, engineering, and mathematics.
                                                <br><br>
                                                Activities include:
                                                <br><br>
                                                > American Indian Science and Engineering Society (AISES) <br>
                                                > STEM presentations in schools that include a planetarium <br>
                                                > Camp Cherokee (STEM Academy) Interactive Summer Camp <br>
                                                > Robotics—Vex Robotics, FIRST Robotics (For Inspiration Recognition of Science and Technology) <br>
                                                > Science Fair—Cherokee Nation Science and Engineering Fair <br>
                                                > American Indian Math and Science Society (AIMS²) <br>
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four7">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Child Protective Services
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Sequoyah Schools is comprised of two separate schools, Cherokee Immersion School and the Sequoyah High School. Each of these schools provides an academic curriculum that focuses on the unique cultural needs of the students.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->

                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four8">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Court Advocacy and Permanency Services
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Performing Arts Camp
                                                <br><br>
                                                Performing Arts Camp is a residential camp for students who will be entering grades 9-12 in the fall. Students work with professional instructors in the areas of acting, singing, and dance in order to prepare them for entering the performing arts. In addition to coursework, students have the opportunity to attend professional performances.
                                                <br><br>
                                                Starbase
                                                <br><br>
                                                Starbase is a STEM summer day camp that is sponsored by the Oklahoma National Guard. The camp includes activities such as the building and launching of rockets, learning about chemistry, physics, geometry, air, and engineering. Registration for this camp is available to students who have just completed the 4th or 5th grades.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four9">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Frequently Asked Questions
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Educations services has a variety of leadership programs that instill cultural pride and develop potential community leaders. 
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four10">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Contact Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <div class="widget-title alt-font text-extra-small text-uppercase margin-15px-bottom font-weight-600">contact information</div>
                                                <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                                                <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                                                <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                    </div>       
                </div>                                
            </div>
        </section>
        <!-- end tab style 04 section -->

<?php
include_once('footer.php');
?>
