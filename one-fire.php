<?php
	include_once('header.php');
?>


        <!-- start tab style 04 section -->
        <section class="wow fadeIn padding-six-tb bg-light-gray" style="margin-top: 123px;">
            <div class="container tab-style4">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12 margin-30px-bottom xs-margin-40px-bottom">
                        <div class="position-relative overflow-hidden width-100">
                            <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> One Fire

                                </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding-right" style="border-right: 1px solid #e5e5e5;">
                        <div class="display-table width-100 height-100">
                            <div class="display-table-cell vertical-align-middle">
                                <!-- start tab navigation -->
                                <ul class="nav nav-tabs alt-font text-uppercase text-small display-inherit font-weight-600">
                                    <li class="active"><a href="#tab-four1" data-toggle="tab">One Fire

                                        </a></li>
                                    <li><a href="#tab-four2" data-toggle="tab">Services</a></li>
                                    <li><a href="#tab-four3" data-toggle="tab">Safety Planning</a></li>
                                    <li><a href="#tab-four4" data-toggle="tab">Domestic Violence Shelters</a></li>
                                    <li><a href="#tab-four5" data-toggle="tab">Contact Us</a></li>
                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 no-padding-left">
                        <div class="tab-content" style="border: 0">
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in active" id="tab-four1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <b>"SILENCE HIDES VIOLENCE" - EMERGENCY HELPLINE 1-866-458-5399</b>
                                                <br><br>
                                                ONE FIRE Victim Services offers a variety of services including housing, legal and advocacy assistance to women who are victims of a crime. The program does not discriminate and offers assistance to those who qualify and reside within the tribe's 14 county jurisdiction.
                                                <br><br>
                                                ONE FIRE's mission is to empower victims seeking help, with the tools they need to rebuild their lives and become the strong individuals they were created to be; to effect social change through outreach and education in order to put an end to inter-generational violence; and to partner with state, county, and tribal courts & law enforcement to hold abusers accountable for their crimes.
                                                <br><br>
                                                ONE FIRE stands for Our Nation Ending Fear, Intimidation, Rape, and Endangerment.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four2">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Services
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                ONE FIRE is a victim services program that is federally funded and receives grants from Victims of Crime Act (VOCA) and Office on Violence Against Women (OVW). ONE FIRE Victim Services does not discriminate and offers a variety of assistance to those who qualify and live within the tribe's 14 county jurisdiction. Services are available to a victim of any race or gender. 
                                                <br><br>
                                                Cherokee Nation ONE FIRE Victim Services office understands that some victims may have needs that exceed the scope of assistance that Cherokee Nation offers and in those cases the tribe may utilize partnerships with the Office on Violence Against Women to meet client needs.
                                            </p>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four3">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Safety Planning
                                                
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                If you have a protective order:
                                                <br><br>
                                                > Keep your protective order (PO) with you at all times. Inform friends, family, neighbors, daycare, school, and employers of this order. <br>
                                                > Call law enforcement to enforce a PO when needed and ensure that you have evidence or witnesses whenever possible. Use a violation log to keep track. <br>
                                                > Tell the child's school and/or child care provider who has permission to pick up the children and the action to take if the abuser or an unapproved person arrives. <br>
                                                > Inform neighbors/family/friends/co-workers/employers that you have filed a PO and what actions they should take if the abuser is seen near you or the children. <br>
                                                > Be prepared to obtain needed legal counsel as soon as possible. Look for someone who has experience with family violence cases. <br>
                                            </p>
  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four4">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Domestic Violence Shelters
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                A domestic violence shelter is a place of temporary refuge and support for victims escaping a violent or abusive situation, such as rape and domestic violence. The location of the shelter is always confidential to ensure security of all participants. 
                                                <br><br>
                                                Some domestic violence shelters have room for both the mother/father and their children. A shelter may provide for all of your basic living needs, including food and childcare. The length of time you can stay at a shelter may be limited. Some shelters may also be able to assist you with finding a permanent home, job, and other things you need to start a new life. Shelters may also be able to refer you to other services within your community.
                                                <br><br>
                                                Cherokee Nation does not currently operate a shelter; however, there are shelters that are within the 14-county Cherokee Nation jurisdiction that we collaborate with.
                                                <br><br>
                                                > Help-in-Crisis 918.456.4357 (Tahlequah) <br>
                                                > SafeNet 918.341.9400 (Claremore) <br>
                                                > DVIS 918.743.5763 (Tulsa) <br>
                                                > Osiyo Men’s Shelter  918.708.9474 (Tahlequah) <br>
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four-1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Updating Your Information
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Have you lost your Cherokee Nation citizenship card or had a name or address change?
                                                <br><br>
                                                If so, complete the Tribal Registration Request Form available on the Downloadable Forms page. The request must contain the following with the completed Tribal Registration Form:
                                                <br><br>
                                                One clear copy of a state-issued driver’s license or state-issued identification card or Cherokee Nation citizenship (blue) card. If using a blue card, be sure it has been signed.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four-1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Talking Leaves Job Corps
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Talking Leaves Job Corps Center opened its doors on October 18, 1978. The center was formerly housed at Northeastern State University’s Loeser Hall. As the enrollment grew and the need for dorm space increased, TLJC moved to the Cherokee Nation in 1988 and later onto their current facility in 1994. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Today, TLJC enjoys a new campus currently serving 197 students and recently received a new five-year contract to continue serving students and assist them in becoming employable for the workforce and equipped for higher education.  
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                For more than four decades, Job Corps has made a difference in the lives of more than 2.6 million economically disadvantaged young Americans. Through a willingness to embrace and welcome change, this voluntary education and job training program continues to offer innovative career technical, academic and social skills training to students at 125 centers nationwide.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TLJC has open enrollment and is currently taking applications. For more information or to apply call 918-456-9959.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four7">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                TERO
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Tribal Employment Rights Office monitors and enforces the Cherokee Nation Tribal TERO Ordinance to ensure employment rights are protected.  Enforcement of employment rights is funded through the Equal Employment Opportunity Commission (E.E.O.C.)  The TERO office will investigate and mediate employment discrimination complaints involving unfair employment procedures or practices prohibited by Title VII, ADA, ADEA, & EPA.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The TERO office also negotiates for job vacancies with contractors doing business with the Cherokee Nation and refers qualified Native American workers to fill those vacancies.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TERO also maintains a list of Native-owned businesses. This list is used by CNE, CNI and the Cherokee Nation Purchasing Department when letting contracts out for bid.  The TERO Ordinance allows for TERO Certified firms to receive preferential treatment in the bid process.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Applicants wishing to apply as a TERO vendor can apply with the TERO office, where the application and all requested documentation will be reviewed. Once the application is complete a site visit will be conducted, and then the applicant will be invited to meet with the TERO committee which decides to certify or not certify a company. If certified, the vendor name will be placed on the TERO vendor list.  If denied certification, vendors many not reapply for a period of one year.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->

                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four8">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Vocational Education and Training
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training, also known as Vocational Education and Training and Career and Technical Education, provides job-specific technical training for work in the trades. These programs generally focus on providing students with hands-on instruction and can lead to certification, a diploma or certificate.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training can also give applicants an edge in job searches since they already have the certifiable knowledge they need to enter the field.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four9">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Vocational Rehabilitation Program
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    The Cherokee Nation Vocational Rehabilitation (CNVR) Program began in 1992 as the first continuous Tribal Vocational Rehabilitation program in the state of Oklahoma. It is a discretionary grant with a five-year grant cycle. Vocational Rehabilitation's fiscal year is from October 1 to September 30. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    CNVR is not a scholarship program but is based on eligibility. If a person is determined eligible, CNVR is able to assist with the cost of training/re-training at universities, colleges, and technology centers to acquire the skills and education needed to become gainfully employed or retain gainful employment.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four5">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Contact Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <div class="widget-title alt-font text-extra-small text-uppercase margin-15px-bottom font-weight-600">contact information</div>
                                                <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                                                <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                                                <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                    </div>       
                </div>                                
            </div>
        </section>
        <!-- end tab style 04 section -->

<?php
include_once('footer.php');
?>
