<?php
	include_once('header.php');
?>

        <!-- start tab style 04 section -->
        <section class="wow fadeIn padding-six-tb bg-light-gray" style="margin-top: 123px;">
            <div class="container tab-style4">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12 margin-30px-bottom xs-margin-40px-bottom">
                        <div class="position-relative overflow-hidden width-100">
                            <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;">Real Estate Services
                                </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding-right" style="border-right: 1px solid #e5e5e5;">
                        <div class="display-table width-100 height-100">
                            <div class="display-table-cell vertical-align-middle">
                                <!-- start tab navigation -->
                                <ul class="nav nav-tabs alt-font text-uppercase text-small display-inherit font-weight-600">
                                    <li class="active"><a href="#tab-four1" data-toggle="tab">Real Estate Services

                                        </a></li>
                                    <li><a href="#tab-four2" data-toggle="tab">Property Leasing</a></li>
                                    <li><a href="#tab-four3" data-toggle="tab">Fee to Trust</a></li>
                                    <li><a href="#tab-four4" data-toggle="tab">Administration of Estates</a></li>
                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 no-padding-left">
                        <div class="tab-content" style="border: 0">
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in active" id="tab-four1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Office of Real Estate Services is responsible for administering the laws, regulations, and policies affecting the protection and management of trust and restricted lands of individual Indian landowners within the 14 county area of the Cherokee Nation and the land owned by Cherokee Nation. Real Estate Services operates under a Self-Governance Compact with the Secretary of the Interior.
                                                <br><br>
                                                Real Estate Services has oversight of approximately 46,000 acres of Restricted land, over 62,000 acres of Tribal Trust land and over 5,100 acres of Tribal Fee land.
                                                <br><br>
                                                > <b>Restricted Land</b> - land the title to which is held by an individual Indian and which can only be alienated or encumbered by the individual Indian owner with the approval of the Secretary of the Interior according to Indian land conveyance provisions and limitations found in Title 25 of the Code of Federal Regulations (CFR). Prior to December 31, 2018, Cherokee allotted lands were held in restricted status only if the owner was at least one-half Indian blood by quantum.  The Amendment of the Stigler Act, passed December 31, 2018, removed the blood quantum provision. <br>
                                                > <b>Trust Land</b> - land held in Trust by the United States of America for an individual Indian refers to land accepted by the federal government to be protected and be under the jurisdiction of the United States of America for the benefit of an individual Indian.  Trust land can only be alienated or encumbered by the individual Indian owner with approval of the Secretary of the Interior, according to the Indian land conveyance.<br>
                                                > <b>Fee Land (or fee simple)</b> - an interest in land that is owned in unrestricted status and is freely alienable by the owner; the owner has full powers of disposition. <br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four2">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Property Leasing
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Agriculture, business and residential leasing on restricted, trust or Cherokee Nation-owned property is handled by Cherokee Nation Real Estate Services. All restricted and trust leases are subject to approval by the Bureau of Indian Affairs, Department of the Interior.
                                                <br><br>
                                                > Majority of agriculture leases are on a 5-year bidding cycle. Interested Lessees should contact Cherokee Nation Real Estate Services to be placed on the list for notification of an upcoming bid event.<br>
                                                > Business leases include commercial, industrial, hunting, or any other profitable business venture sitting on Indian land.<br>
                                                > Residential leases<br>
                                            </p>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four3">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Fee to Trust
                                                
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Fee to Trust Applications. This is a transfer of land from an eligible Indian tribe or eligible Indian individual to the United States of America, in trust, for the benefit of the eligible Indian tribe or eligible Indian individual.
                                           </p>
  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four4">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Administration of Estates
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The administration of an estate, also known as probating an estate, is a legal process commenced after a person’s death to determine the deceased’s assets, the value of those assets, and the distribution of those assets, according to law.
                                                <br><br>
                                                <b>Restricted property</b>
                                                If the deceased owned restricted or fee property, it must be probated separately in the District Court of Oklahoma in the county where the deceased lived.  The Real Estate Services office of Cherokee Nation can assist with this process.
                                                <br><br>
                                                <b>Trust property</b>
                                                If the deceased owned trust land, the process to convey to successive generations is administered by the Office of Hearings and Appeals of the Department of the Interior. The Real Estate Services office of Cherokee Nation can assist with this process.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four-1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Updating Your Information
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Have you lost your Cherokee Nation citizenship card or had a name or address change?
                                                <br><br>
                                                If so, complete the Tribal Registration Request Form available on the Downloadable Forms page. The request must contain the following with the completed Tribal Registration Form:
                                                <br><br>
                                                One clear copy of a state-issued driver’s license or state-issued identification card or Cherokee Nation citizenship (blue) card. If using a blue card, be sure it has been signed.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four-1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Talking Leaves Job Corps
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Talking Leaves Job Corps Center opened its doors on October 18, 1978. The center was formerly housed at Northeastern State University’s Loeser Hall. As the enrollment grew and the need for dorm space increased, TLJC moved to the Cherokee Nation in 1988 and later onto their current facility in 1994. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Today, TLJC enjoys a new campus currently serving 197 students and recently received a new five-year contract to continue serving students and assist them in becoming employable for the workforce and equipped for higher education.  
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                For more than four decades, Job Corps has made a difference in the lives of more than 2.6 million economically disadvantaged young Americans. Through a willingness to embrace and welcome change, this voluntary education and job training program continues to offer innovative career technical, academic and social skills training to students at 125 centers nationwide.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TLJC has open enrollment and is currently taking applications. For more information or to apply call 918-456-9959.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four7">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                TERO
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Tribal Employment Rights Office monitors and enforces the Cherokee Nation Tribal TERO Ordinance to ensure employment rights are protected.  Enforcement of employment rights is funded through the Equal Employment Opportunity Commission (E.E.O.C.)  The TERO office will investigate and mediate employment discrimination complaints involving unfair employment procedures or practices prohibited by Title VII, ADA, ADEA, & EPA.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The TERO office also negotiates for job vacancies with contractors doing business with the Cherokee Nation and refers qualified Native American workers to fill those vacancies.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TERO also maintains a list of Native-owned businesses. This list is used by CNE, CNI and the Cherokee Nation Purchasing Department when letting contracts out for bid.  The TERO Ordinance allows for TERO Certified firms to receive preferential treatment in the bid process.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Applicants wishing to apply as a TERO vendor can apply with the TERO office, where the application and all requested documentation will be reviewed. Once the application is complete a site visit will be conducted, and then the applicant will be invited to meet with the TERO committee which decides to certify or not certify a company. If certified, the vendor name will be placed on the TERO vendor list.  If denied certification, vendors many not reapply for a period of one year.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->

                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four8">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Vocational Education and Training
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training, also known as Vocational Education and Training and Career and Technical Education, provides job-specific technical training for work in the trades. These programs generally focus on providing students with hands-on instruction and can lead to certification, a diploma or certificate.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training can also give applicants an edge in job searches since they already have the certifiable knowledge they need to enter the field.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four9">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Vocational Rehabilitation Program
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    The Cherokee Nation Vocational Rehabilitation (CNVR) Program began in 1992 as the first continuous Tribal Vocational Rehabilitation program in the state of Oklahoma. It is a discretionary grant with a five-year grant cycle. Vocational Rehabilitation's fiscal year is from October 1 to September 30. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    CNVR is not a scholarship program but is based on eligibility. If a person is determined eligible, CNVR is able to assist with the cost of training/re-training at universities, colleges, and technology centers to acquire the skills and education needed to become gainfully employed or retain gainful employment.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four5">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Contact Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <div class="widget-title alt-font text-extra-small text-uppercase margin-15px-bottom font-weight-600">contact information</div>
                                                <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                                                <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                                                <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                    </div>       
                </div>                                
            </div>
        </section>
        <!-- end tab style 04 section -->

<?php
include_once('footer.php');
?>
