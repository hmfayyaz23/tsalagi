<?php
	include_once('header.php');
?>

        <!-- start tab style 04 section -->
        <section class="wow fadeIn padding-six-tb bg-light-gray" style="margin-top: 123px;">
            <div class="container tab-style4">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12 margin-30px-bottom xs-margin-40px-bottom">
                        <div class="position-relative overflow-hidden width-100">
                            <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Tag Office
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding-right" style="border-right: 1px solid #e5e5e5;">
                        <div class="display-table width-100 height-100">
                            <div class="display-table-cell vertical-align-middle">
                                <!-- start tab navigation -->
                                <ul class="nav nav-tabs alt-font text-uppercase text-small display-inherit font-weight-600">
                                    <li class="active"><a href="#tab-four1" data-toggle="tab">Tag Office
                                    </a></li>
                                    <li><a href="#tab-four2" data-toggle="tab">Vehicle Tags</a></li>
                                    <li><a href="#tab-four3" data-toggle="tab">Registration</a></li>
                                    <li><a href="#tab-four4" data-toggle="tab">Online Tag Renewal</a></li>
                                    <li><a href="#tab-four5" data-toggle="tab">Downloadable Forms</a></li>
                                    <li><a href="#tab-four6" data-toggle="tab">Fee Schedule</a></li>
                                    <li><a href="#tab-four7" data-toggle="tab">VIN Inspection</a></li>
                                    <li><a href="#tab-four8" data-toggle="tab">Merchandise</a></li>
                                    <li><a href="#tab-four9" data-toggle="tab">Confidentiality</a></li>
                                    <li><a href="#tab-four10" data-toggle="tab">Liens</a></li>
                                    <li><a href="#tab-four11" data-toggle="tab">Rules and Regulations</a></li>
                                    <li><a href="#tab-four12" data-toggle="tab">Repossession</a></li>
                                    <li><a href="#tab-four13" data-toggle="tab">Contact Us</a></li>

                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 no-padding-left">
                        <div class="tab-content" style="border: 0">
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in active" id="tab-four1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Online Tag Renewal
                                                </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    The following vehicle types are eligible for online renewal at this time:
                                                    <br><br>
                                                    > Standard Automobiles and Trucks, including personal tags.<br>
                                                    To register online, the following information will be required:
                                                    <br><br>
                                                    > Last 4 characters of the vehicle identification number (VIN)<br>
                                                    > The license plate number<br>
                                                    > Tribal Membership Number<br>
                                                    > Current liability insurance information<br>
                                                    > Payment by Credit Card: VISA, MasterCard, American Express, Discover Card<br><br>
                                                    Online renewal is allowed beginning the 1st day of the registration expiration month.<br>
                                                    <br><br>
                                                    Penalties will be calculated for overdue tags at secure checkout.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four2">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Vehicle Tags                                                </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Cherokee Nation currently provides all motor vehicle tags including commercial vehicles, farm trucks, military service, personalized and specialty tags, physically disabled, as well as RV and travel trailers.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four3">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Registration
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                            The Cherokee Nation provides registration for all Motor Vehicles including, Farm Trucks, Commercial Vehicles, Private Trailers, Physically Disabled, Boats and Motors, RV's, Travel Trailers, as well as ATVs. At Large Citizens residing in the State of Oklahoma may also register their vehicles at the same rate as Oklahoma Motor Vehicle Division with a 10% discount.
                                                <br><br>
                                            If you are in need of a duplicate registration you may request one, from one of our 6 locations or by completing the vehicle information request form and mailing the form to the Tag Office along with the $1.00 fee. Check or money orders are accepted, do not send cash.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four4">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Online Tag Renewal

                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    The following vehicle types are eligible for online renewal at this time:
                                                <br><br>
                                                    Standard Automobiles and Trucks, including personal tags.
                                                    To register online, the following information will be required:
                                                    <br><br>
                                                    Last 4 characters of the vehicle identification number (VIN) <br>
                                                    The license plate number<br>
                                                    Tribal Membership Number
                                                    Current liability insurance information<br>
                                                    Payment by Credit Card: VISA, MasterCard, American Express, Discover Card<br>
                                                    Online renewal is allowed beginning the 1st day of the registration expiration month.<br>
                                                    <br><br>
                                                    Penalties will be calculated for overdue tags at secure checkout.
                                            </p>

                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four5">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Downloadable Forms
                                            </h6>
                                            <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                    <p class="no-margin-bottom public-notices">
                                                        Downloads
                                                    </p>
                                                    
                                                    
                                                    <p class="no-margin-bottom downloadPDF" >
                                                        <a href="https://tagoffice.cherokee.org/media/b1qntjqu/01-01-appl-for-cn-cert-of-title_062018.pdf" style="border-bottom: 1px solid blue;">
                                                            01-01 Application for Certificate of Title
                                                        </a> <br/>
                                                        <span style="font-size: 14px; font-style: italic;"> 69.9 KB -- Updated:5/10/2019</span>
                                                        <br><br>
                                                        <span> Cherokee Nation Tax Commission application for a certificate of title for a vehicle.
                                                            </span>
                                                    </p>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four6">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Fee Schedule

                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                            Motor Vehicle Title, Registration, and Miscellaneous Fees
                                            <br><br>
                                            To verify fees not listed or for clarification contact the Tag Office 918-453-5100. Rules & Fees in this section apply to Compact Jurisdictional Area. 
                                            </p>
                                            <br>
                                            <table >

                                            <tr>
                                                <th>Title Fees</th>
                                            </tr>
                                            <tr>
                                                <td>Titles</td>
                                                <td>$6.00</td>
                                            </tr>
                                            </table>
                                            <br>
                                            <table >
                                            <tr>
                                                <th>Registration Fees	</th>
                                            </tr>
                                            <tr>
                                                <td>Regular Registration Years 1-4</td>
                                                <td>$75.00</td>
                                            </tr>
                                            <tr>
                                                <td>Regular Registration Years 5-8</td>
                                                <td>$65.00</td>
                                            </tr>
                                            <tr>
                                                <td>Regular Registration Years 9-12</td>
                                                <td>$45.00</td>
                                            </tr>
                                            <tr>
                                                <td>Regular Registration Years 13-16</td>
                                                <td>$25.00</td>
                                            </tr>
                                            <tr>
                                                <td>Regular Registration Years 17+</td>
                                                <td>$10.00</td>
                                            </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four7">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    VIN Inspection

                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                All previously registered vehicles, regardless of age, entering Cherokee Nation from Oklahoma, another state, another tribe, or territory, must be physically inspected before an original Cherokee Nation title may be issued.
                                                <br><br>
                                                Option 1: Inspection can be done by a Cherokee Nation Motor Vehicle Division Agent or Cherokee Nation Tax Commission Personnel - $2 fee
                                                <br><br>
                                                Option 2: Applicant Self Inspection forms may be completed, notarized and submitted along with a $2 processing fee. In order for Cherokee Nation Tax Commission Agent to Notarized (attest or witness) form must be signed in the presence of an agent.
                                                <br><br>
                                                Self Vin Inspection Form 82-01 may be found in the Downloadable Forms section.
                                                <br><br>
                                                A non-complying form will be rejected; for example, incomplete, erasures, strikeovers, whiteout, and or illegible information.
                                                <br><br>
                                                List "N/A" in the odometer reading section of the Application when inspecting a travel trailer.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->

                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four8">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Merchandise
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training, also known as Vocational Education and Training and Career and Technical Education, provides job-specific technical training for work in the trades. These programs generally focus on providing students with hands-on instruction and can lead to certification, a diploma or certificate.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four9">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Confidentiality
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Information contained in Cherokee Nation Tax Commission vehicle/boat/outboard records are confidential under both federal and tribal rules and regulations. The information may be released only in certain situations to certain requestors. To apply for such information, the requestor must complete a VEHICLE INFORMATION REQUEST FORM, denoting their reason for requesting the information. If the reason qualifies under the statutory criteria, the information may be released for a fee ranging from $1.00 to $10.00, depending upon the type or information requested.
                                                <br><br>
                                                The updated VEHICLE INFORMATION REQUEST FORM containing instructions and a list of acceptable request criteria is available for download on this website.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four10">
                                    <div class="row equalize xs-equalize-auto">
                                        <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                            <div class="display-table-cell vertical-align-middle">
                                                <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                        Liens
                                                </h6>
    
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    To record a security interest on a newly purchased vehicle or a vehicle changing ownership, a secured party must complete and submit a CHEROKEE NATION LIEN ENTRY FORM along with the assigned Manufacturer’s Certificate of Origin (new vehicle) or title (used vehicle) and the lien filing fee of $10.00 to Cherokee Nation Tax Commission. The lien will be processed and the appropriate forms returned to the secured party. The secured party will return the appropriate documents to the debtor with a notice to title and register the vehicle within thirty (30) days. Upon payment of all applicable transfer taxes and fees, a Cherokee Nation title will be issued to the record owner, reflecting the perfected lien.
                                                    <br><br>
                                                    The above lien filing procedure also applies when a lien is being perfected on a vehicle that is not changing ownership. The lien filing fee is $10.00, in addition to the title fee of $6.00. A new Cherokee Nation title reflecting the perfected lien will be issued to the record owner.
                                                    <br><br>
                                                    Refer to LIEN FILING AND RELEASE GUIDELINES in the Downloadable Forms Section of this site for downloadable instructions concerning the above procedures.
                                                    <br><br>
                                                    To release a lien, the lienholder must sign and date two (2) release forms. Mail one (1), signed and dated copy of a lien release to Cherokee Nation Tax Commission P.O. Box 948 Tahlequah, OK 74465; and one (1) signed and dated copy of the lien release to the debtor.
                                                    <br><br>
                                                    When a lien release cannot be obtained, a notarized statement from the secured party may be accepted. Refer to LIEN RELEASE AFFIDAVIT LETTER or AFFIDAVIT OF LIEN RELEASE in the DOWNLOADABLE FORMS SECTION of this site for downloadable instructions on this process.
                                                    <br><br>
                                                    Please note all information on lien entry forms must be typed, or computer generated. Forms must be legible. Forms with illegible information will not be accepted.
                                                    <br><br>
                                                    LIEN ENTRY FORM 69-01 is specifically formatted for personal computer entry. Available for downloading from the Downloadable Forms section of this site. Non-complying forms will be rejected.
                                                    <br><br>
                                                    Three (3) identical copies of the completed lien entry form are to be generated by the lender and submitted to the Cherokee Nation Tax Commission.
                                             </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end tab content -->
                                <!-- start tab content -->
                                <div class="tab-pane med-text fade in" id="tab-four11">
                                        <div class="row equalize xs-equalize-auto">
                                            <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                                <div class="display-table-cell vertical-align-middle">
                                                    <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                            Rules and Regulations

                                                    </h6>

                                                    <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                            The Commission shall adopt Rules & Regulations and amendments as it deems necessary to enforce Cherokee Nation Motor Vehicle and Boat & Motor Tax Code.
                                                </p>
                                                <br>
                                                <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                    <p class="no-margin-bottom public-notices">
                                                        Downloads
                                                    </p>
                                                    
                                                   
                                                    <p class="no-margin-bottom downloadPDF" >
                                                        <a href="https://tagoffice.cherokee.org/media/j4db2bud/boatmotorrulesandregulatons_july2015.pdf" style="border-bottom: 1px solid blue;">
                                                            Boat Motor Rules and Regulations
                                                        </a> <br/>
                                                        <span style="font-size: 14px; font-style: italic;">  878.8 KB -- Updated:5/13/2019</span>
                                                        <br><br>
                                                        <span> Cherokee Nation Tax Commission Motor Vehicle Division, Boat and Motor rules and regulations as of July 2015.
                                                            </span>
                                                    </p>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end tab content -->
                                                                    <!-- start tab content -->
                                <div class="tab-pane med-text fade in" id="tab-four12">
                                        <div class="row equalize xs-equalize-auto">
                                            <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                                <div class="display-table-cell vertical-align-middle">
                                                    <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                            Repossession

                                                    </h6>

                                                    <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                            Repossession Fee $55.00
                                                            <br><br>
                                                            Repossession Affidavit may be found in the downloadable forms section.
                                                            <br><br>
                                                            The repossessing lender must provide the following documentation in order for the Repossession Application to be approved:
                                                            <br><br>
                                                            1. Properly completed Repossession Affidavit.
                                                            <br><br>
                                                            Tribal Repossession Titles and Oklahoma Tax Commission Motor Vehicle Division (August 19, 2002).  If the Repossession Title is in the name of the lending institution only and it is a tribal title OTC will charge back taxes. When a tribal title has been issued in the name of a lending institution and an individual tribal member, OTC will allow the lending institution to assign ownership to themselves, provided they submit verification the individual listed on the tribal title was a member of the title issuing tribe.
                                                            <br><br>
                                                            If the tribal title presented has language on its face declaring that issuance of the title confirms membership of the titled owner in that tribe, OTC will accept the title with no additional membership verification required. “It’s hereby certified according to the records of the Cherokee Nation, the person named herein is a Citizen of this Nation”
                                                            <br><br>
                                                            2. Actual or certified copy or chattel mortgage, conditional sales contract or other type security agreement.
                                                            <br><br>
                                                            Note:  Contract must indicate the vehicle, boat, or outboard motor has been pledged as collateral as lists a complete description of the unit along with a vehicle identification number or hull identification number.
                                                            <br><br>
                                                            3. An acceptable, signed and dated Cherokee Nation Lien Release Form: Which can consist of one of these 4 options: Copy #1, #2, or #3 of Lien Release Form 75-01 OR BM-75 OR Certified Copy of Copy #1, #2, or #3 of Lien Release OR A Lien Release Affidavit Letter OR Copy #3 (blue) and Copy #4 (green) Form 2.11 Lien Release OR Certified Copy of #1 Form 2.11.  If more than one (1) lien holder, the following is also required:
                                                            <br><br>
                                                            A copy of a certified letter from the repossession lender notifying the second lien holder of the intent to repossess.
                                                            <br><br>
                                                            Letter must be dated at least ten (10) days prior to the date of the repossession affidavit is processed by the Cherokee Nation motor license agent or the Motor Vehicle Division.
                                                            <br><br>
                                                            The post office receipt or the return receipt (green card) signed by the second lienholder is required as proof of mailing.
                                                            <br><br>
                                                            If the second lienholder is making application for a repossession title, signed and dated lien release Copy #1, #2, or #3 of Lien Release Form 75-01 OR BM-75 OR Certified Copy of Copy #1, #2, or #3 of Lien Release OR A Lien Release Affidavit Letter OR Copy #3 (blue) and Copy #4 (green) Form 2.11 Lien Release OR Certified Copy of #1 Form 2.11.
                                                            <br><br>
                                                            4. If a motor vehicle, Insurance Security Verification Form reflecting all required information or a Cherokee Nation Tax Commission Form 31-01-Affidavit of Non-Use in Lieu of Liability Insurance.
                                                            Please direct any questions to the Cherokee Nation Tax Commission Motor Vehicle Division at 918-453-5100.
                                                   </p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four13">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Contact Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <div class="widget-title alt-font text-extra-small text-uppercase margin-15px-bottom font-weight-600">contact information</div>
                                                <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                                                <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                                                <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                    </div>       
                </div>                                
            </div>
        </section>
        <!-- end tab style 04 section -->

<?php
include_once('footer.php');
?>
