<?php
	include_once('header.php');
?>


        <!-- start tab style 04 section -->
        <section class="wow fadeIn padding-six-tb bg-light-gray" style="margin-top: 123px;">
            <div class="container tab-style4">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12 margin-30px-bottom xs-margin-40px-bottom">
                        <div class="position-relative overflow-hidden width-100">
                            <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Community & Cultural Outreach </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding-right" style="border-right: 1px solid #e5e5e5;">
                        <div class="display-table width-100 height-100">
                            <div class="display-table-cell vertical-align-middle">
                                <!-- start tab navigation -->
                                <ul class="nav nav-tabs alt-font text-uppercase text-small display-inherit font-weight-600">
                                    <li class="active"><a href="#tab-four1" data-toggle="tab">Community & Cultural Outreach </a></li>
                                    <li><a href="#tab-four2" data-toggle="tab">Community and Cultural Outreach Conference</a></li>
                                    <li><a href="#tab-four3" data-toggle="tab">Downloads  </a></li>
                                    <li><a href="#tab-four4" data-toggle="tab">Grants</a></li>
                                    <li><a href="#tab-four5" data-toggle="tab">Programs</a></li>
                                    <li><a href="#tab-four6" data-toggle="tab">Contact Us</a></li>
                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 no-padding-left">
                        <div class="tab-content" style="border: 0">
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in active" id="tab-four1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <b>Mission</b>
                                                <br><br>
                                                The Community & Cultural Outreach Program’s mission is to assist faith and community-based organization's ability to increase their effectiveness; enhance essential services to those most in need, and build upon the organizational capacity of each community, diversify resources, and create collaborations to serve those in Cherokee communities.
                                                <br><br>
                                                <b>Purpose</b>
                                                <br><br>
                                                Community & Cultural Outreach was designed to facilitate opportunities for partnerships between Cherokee Nation and its communities within the 14-county tribal jurisdiction, as a self-help catalyst to education and provide technical assistance empowering communities to utilize their own abilities in securing and administering general and federal funding opportunities.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four2">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Community and Cultural Outreach Conference                                                </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Community & Cultural Outreach is proud to announce its 16th annual Conference of Community Leaders which will be held June 12 & 13, 2020 at the new Cherokee Casino and Chota Conference Center in Tahlequah, Oklahoma. 
                                                <br><br>
                                                Every year, CCO brings together Cherokee community leaders from the 14-county tribal jurisdictional area as well as the 24 satellite communities across the United States for two days of learning, networking, and interaction. The conference consists of capacity building, Cherokee culture, Cherokee history, and youth leadership sessions, all led by professionals from across the country.
                                                <br><br>
                                                The theme of this year’s conference is Servant Leadership. Every organization is encouraged to bring 2-3 youth participants who are ages 14 to 20 to participate in the youth sessions.
                                                <br><br>
                                                In addition, CCO will provide three meals per day, and many activities including a celebration on Friday following dinner and an awards banquet concluding the conference on Saturday evening.  
                                                <br><br>
                                                Due to limited space, registration will be limited to 500 attendees. Please register early and only register if you are sure you will be able to attend. Registration will open March 1 and run until May 1st or until the max number of participants is reached.
                                                <br><br>
                                                For more information, please contact 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four3">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Downloads
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                These documents are applications and supporting documents for grants within Community and Cultural Outreach.                                            </p>
                                            <br>
                                            <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                <p class="no-margin-bottom public-notices">
                                                    Downloads
                                                </p>
                                                
                                                <p class="no-margin-bottom downloadPDF" >
                                                    <a href="https://www.cherokee.org/media/szbel2me/neworgapp.pdf" style="border-bottom: 1px solid blue;">
                                                        New Organization Grant Application
                                                    </a> <br/>
                                                    <span style="font-size: 14px; font-style: italic;"> 325.8 KB -- Updated:2/25/2020</span>
                                                    <br><br>
                                                    <span>This application is for organizations that have never received assistance with COTTA before.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four4">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Grants
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;font-weight: 600">
                                                In District Grants
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                > Sustainability Grant <br>
                                                This grant, funded by the Housing, Jobs and Sustainable Communities Act, is meant for funding for such green-friendly efforts as, upgrading air-conditioning/heating systems, internet connectivity, insulation, windows, and other cost-saving renewable energy technology in Cherokee Community Buildings.
                                                <br><br>
                                                > Community Work Grant <br>
                                                The Community Work Program Grant fund was established from tribally generated revenue and support for eligible community organizations in the Cherokee Nation Tribal Jurisdiction Service Area. The program is designed to be responsive to the needs of Cherokee communities organized around issues and projects with broad community support.
                                                <br><br>
                                                The objective of the Community Work Project is to improve the capacity of Cherokee communities to thrive as healthy, culturally-intact and economically self-reliant communities for future generations.
                                        </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four5">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Programs
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Cultural Outreach/At-Large Program
                                                <br><br>
                                                The Cultural Outreach staff is focused on providing assistance to our Cherokee citizens that live outside our traditional service area. CCO Staff provides cultural and historical information as well as coordinating visits to the communities from the Chief and local community activities.
                                                <br><br>
                                                Cultural Enrichment
                                                <br><br>
                                                The camp immersed Cherokee at-large community leadership in Cherokee culture. They learned about Cherokee material goods and how they were produced. Classes covered Cherokee spiritual beliefs and how those translated into daily living. Participants also attended Cherokee language classes. To help build a spirit of community, one evening was spent fellowshipping with leaders from the local community organizations. We have begun, from this activity, building a network of Cherokee sister communities across North American, tying at-large communities to communities within our homeland.
                                                <br><br>
                                                Volunteer Program
                                                <br><br>
                                                The Volunteer Program was established from tribally generated revenue and support. Services are accessible to eligible Cherokee Nation citizens or community organizations in the Cherokee Nation Tribal Jurisdictional Service Area as funding and resources become available.
                                                <br><br>
                                                The mission of the Volunteer Program is to serve our elderly and/or disabled Cherokee citizens in need by partnering with various organizations and individuals in securing the labor, tools, equipment, and materials necessary to create safer living conditions.
                                                <br><br>
                                                Cherokee Language Master Apprentice Program
                                                <br><br>
                                                The concept was to match first language Cherokee speakers (Masters) with those who wished to learn Cherokee as a second language (Apprentice). These Apprentices would then become Masters and perpetuate the cycle, increasing the number of Cherokee Speakers exponentially.
                                                <br><br>
                                                The mission of this program is to develop fluent Cherokee Speakers, who become teachers, by surrounding the Apprentices in the Cherokee culture and language.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four-1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Links
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                > Legal Aid of Oklahoma <br>
                                                > DHS child support calculator<br>
                                                > Oklahoma Indian Legal Services<br>
                                                > Oklahoma Works<br>
                                                > OKDHS Child Support<br>
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four7">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Payment Information
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Most parents pay child support by payroll deduction. Employers will receive an income withholding order that alerts them to withhold the support amount from the paycheck of parents who are ordered to pay support and forward it to the Cherokee Nation Child Support Payment Center. Once the payment center receives the payment, CNOCSS prepares a check and mails it the custodial parent.
                                                <br><br>
                                                If a parent is not able to pay by payroll deduction, the payment can be mailed in or made in person at the Cherokee Nation Office of Child Support Services. Payments can only be accepted in the form of a money order or cashier’s check, cash is not accepted.
                                                Payments submitted after 2 P.M. will not be processed until the following business day.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->

                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four8">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Tip Line
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Currently, the Cherokee Nation Office of Child Support Services (CNOCSS) has many cases in which the non-custodial parent is not providing financial support to their children.
                                                <br><br>
                                                In an effort to improve the lives of children, CNOCSS has established an anonymous tip line to help locate non-custodial parents. If you have any information about someone that CNOCSS is looking for, please email the following information to: childsupporttipline@cherokee.org
                                                <br><br>
                                                The information that you provide will be verified. Once the information has been verified, CNOCSS can use the information in several ways:
                                                <br><br>
                                                To file a case in court <br>
                                                To serve a non-custodial parent with court documents <br>
                                                To enforce a court order <br>
                                                To provide information on this website, you must know the first and last name of the non-custodial parent in the case. <br>
                                                When providing information, please give as many details as possible. Please be aware that anyone providing information to the tip line will remain anonymous.
                                                <br><br>
                                                Please provide the following information for the non-custodial parent in question:
                                                <br><br>
                                                First name: <br>
                                                Middle name: <br>
                                                Last name: <br>
                                                Alias: <br>
                                                Address: <br>
                                                City: <br>
                                                State: <br>
                                                Zip Code: <br>
                                                Phone ( ) <br>
                                                Date of Birth: (example: MM-DD-YYYY) <br>
                                                Description of vehicle: <br>
                                                <br><br>
                                                Possible employment (name of employer and address):
                                                <br><br>
                                                Any additional information that will assist CNOCSS in locating the Non-custodial parent:
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four9">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Why Choose Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Call Us Today 866-247-5346 or 918-453-5444
                                                <br><br>
                                                We know you have options when it comes to collecting your unpaid child support. When you choose Cherokee Nation Office of Child Support Services (CN OCSS) you choose to have the best representation possible to collect your unpaid child support.
                                                <br><br>
                                                In 2018, CN OCSS collected $3,510,288.00 in child support payments. By choosing CN OCSS you choose to enforce your rights for free, we are unlike other agencies and do not collect any fees.
                                             </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four6">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Contact Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <div class="widget-title alt-font text-extra-small text-uppercase margin-15px-bottom font-weight-600">contact information</div>
                                                <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                                                <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                                                <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                    </div>       
                </div>                                
            </div>
        </section>
        <!-- end tab style 04 section -->

<?php
include_once('footer.php');
?>
