<?php
	include_once('header.php');
?>

        <!-- start tab style 04 section -->
        <section class="wow fadeIn padding-six-tb bg-light-gray" style="margin-top: 123px;">
            <div class="container tab-style4">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12 margin-30px-bottom xs-margin-40px-bottom">
                        <div class="position-relative overflow-hidden width-100">
                            <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Human Services
                                </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding-right" style="border-right: 1px solid #e5e5e5;">
                        <div class="display-table width-100 height-100">
                            <div class="display-table-cell vertical-align-middle">
                                <!-- start tab navigation -->
                                <ul class="nav nav-tabs alt-font text-uppercase text-small display-inherit font-weight-600">
                                    <li class="active"><a href="#tab-four1" data-toggle="tab">Human Services
                                        </a></li>
                                    <li><a href="#tab-four2" data-toggle="tab">Family Assistance</a></li>
                                    <li><a href="#tab-four3" data-toggle="tab">Youth Services & Special Projects</a></li>
                                    <li><a href="#tab-four4" data-toggle="tab">Contact Us</a></li>
                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 no-padding-left">
                        <div class="tab-content" style="border: 0">
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in active" id="tab-four1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Cherokee Nation Human Services provides several assistance programs to qualifying families. These programs include assistance with housing, food and nutrition, child care and development, child support and elder assistance. Many of these programs are for emergency, short-term needs that may not be covered in other Cherokee Nation programs. The programs provided by Human Services focus on stabilization of families and promote self-sufficiency. 
                                          </p>
                                          <br>
                                          <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                            <p class="no-margin-bottom public-notices">
                                                Public Notices
                                            </p>

                                            <p class="no-margin-bottom downloadPDF" >
                                                <a href="https://www.cherokee.org/media/w3yi35rg/2020-liheap-public-notice.pdf" style="border-bottom: 1px solid blue;">
                                                    2020 LIHEAP Application
                                                </a> <br/>
                                                <span style="font-size: 14px; font-style: italic;"> 415.2 KB -- Created:7/31/2019  |  Updated:7/31/2019 </span>
                                                <br><br>
                                                <span>Cherokee Nation is currently in the process of submitting an application to the U.S. Department of Health and Human Services for a LIHEAP grant. </span>
                                            </p>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four2">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Family Assistance
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Cherokee Nation Family Assistance offers a variety of services and continues to develop and expand programs that provide critical services to families, elders and veterans that promote family unity and self-sufficiency. Programs include LIHEAP, food distribution, burial assistance, child support services, veteran services, elder services and more.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four3">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Youth Services & Special Projects
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Youth Services & Special Projects services are open to youth and families of all races and ethnicities. Along with youth shelter services, outreach programs involve work with communities, schools, and individuals on topics such as domestic, dating, and family violence. Services are also available for youth who are victims of a crime.                                            </p>
                                            <br/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four-1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Genealogy Information
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Note: The research information and links below are not maintained by the Cherokee Nation.
                                                <br><br>
                                                To search the Guion Miller and Dawes/Freedman rolls, visit:
                                                <br><br>
                                                Dawes/Freedman Roll Search – NARA Archival Information Locator (NAIL): www.archives.gov
                                                <br><br>
                                                Dawes Rolls can also be searched at Access Geneology
                                                <br><br>
                                                The Cherokee Heritage Center has a genealogist available to assist in researching Cherokee ancestry for a fee. Call 918-456-6007 visit www.cherokeeheritage.org.
                                                <br><br>
                                                Those tracing an ancestor who came from Tennessee, Georgia, eastern Alabama or the South Carolina area may want to correspond with: 
                                                <br><br>
                                                The Eastern Band of Cherokee Indians<br>
                                                Qualla Boundary<br>
                                                PO Box 455<br>
                                                Cherokee, NC 28719<br>
                                                828-497-4771<br>
                                                https://ebci.com<br>
                                                <br><br>
                                                If you need further genealogy assistance at other times, the Muskogee Public Library, 801 West Okmulgee in Muskogee, Okla., may be able to help. Call 918-682-6657. It contains most of the Cherokee Dawes applications and the Miller Roll applications, as well as additional federal census records. 
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four5">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Updating Your Information
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Have you lost your Cherokee Nation citizenship card or had a name or address change?
                                                <br><br>
                                                If so, complete the Tribal Registration Request Form available on the Downloadable Forms page. The request must contain the following with the completed Tribal Registration Form:
                                                <br><br>
                                                One clear copy of a state-issued driver’s license or state-issued identification card or Cherokee Nation citizenship (blue) card. If using a blue card, be sure it has been signed.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four-1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Talking Leaves Job Corps
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Talking Leaves Job Corps Center opened its doors on October 18, 1978. The center was formerly housed at Northeastern State University’s Loeser Hall. As the enrollment grew and the need for dorm space increased, TLJC moved to the Cherokee Nation in 1988 and later onto their current facility in 1994. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Today, TLJC enjoys a new campus currently serving 197 students and recently received a new five-year contract to continue serving students and assist them in becoming employable for the workforce and equipped for higher education.  
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                For more than four decades, Job Corps has made a difference in the lives of more than 2.6 million economically disadvantaged young Americans. Through a willingness to embrace and welcome change, this voluntary education and job training program continues to offer innovative career technical, academic and social skills training to students at 125 centers nationwide.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TLJC has open enrollment and is currently taking applications. For more information or to apply call 918-456-9959.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four7">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                TERO
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Tribal Employment Rights Office monitors and enforces the Cherokee Nation Tribal TERO Ordinance to ensure employment rights are protected.  Enforcement of employment rights is funded through the Equal Employment Opportunity Commission (E.E.O.C.)  The TERO office will investigate and mediate employment discrimination complaints involving unfair employment procedures or practices prohibited by Title VII, ADA, ADEA, & EPA.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The TERO office also negotiates for job vacancies with contractors doing business with the Cherokee Nation and refers qualified Native American workers to fill those vacancies.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TERO also maintains a list of Native-owned businesses. This list is used by CNE, CNI and the Cherokee Nation Purchasing Department when letting contracts out for bid.  The TERO Ordinance allows for TERO Certified firms to receive preferential treatment in the bid process.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Applicants wishing to apply as a TERO vendor can apply with the TERO office, where the application and all requested documentation will be reviewed. Once the application is complete a site visit will be conducted, and then the applicant will be invited to meet with the TERO committee which decides to certify or not certify a company. If certified, the vendor name will be placed on the TERO vendor list.  If denied certification, vendors many not reapply for a period of one year.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->

                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four8">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Vocational Education and Training
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training, also known as Vocational Education and Training and Career and Technical Education, provides job-specific technical training for work in the trades. These programs generally focus on providing students with hands-on instruction and can lead to certification, a diploma or certificate.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training can also give applicants an edge in job searches since they already have the certifiable knowledge they need to enter the field.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four9">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Vocational Rehabilitation Program
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    The Cherokee Nation Vocational Rehabilitation (CNVR) Program began in 1992 as the first continuous Tribal Vocational Rehabilitation program in the state of Oklahoma. It is a discretionary grant with a five-year grant cycle. Vocational Rehabilitation's fiscal year is from October 1 to September 30. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    CNVR is not a scholarship program but is based on eligibility. If a person is determined eligible, CNVR is able to assist with the cost of training/re-training at universities, colleges, and technology centers to acquire the skills and education needed to become gainfully employed or retain gainful employment.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four4">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Contact Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <div class="widget-title alt-font text-extra-small text-uppercase margin-15px-bottom font-weight-600">contact information</div>
                                                <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                                                <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                                                <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                    </div>       
                </div>                                
            </div>
        </section>
        <!-- end tab style 04 section -->

<?php
include_once('footer.php');
?>
