<?php
	include_once('header.php');
?>

        <!-- start slider section  -->
        <section class="wow fadeIn no-padding main-slider mobile-height top-space" >
            <div class="swiper-full-screen swiper-container width-100 white-move">
                <div class="swiper-wrapper">
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('images/1.jpg');">
                        <div class="opacity-extra-medium bg-extra-dark-gray" style="background: linear-gradient(to right, rgba(0,0,0,0.0) 0%,rgba(0,0,0,0.8) 78%,rgba(0,0,0,0.8) 100%)"></div>
                        <div class="container position-relative full-screen xs-height-400px">
                            <div class="slider-typography" style="text-align: right">
                                <div class="slider-text-middle-main">
                                    <div class="slider-text-middle">
                                        <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 42px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 500;">
                                            Find Your Way 
                                        </span>
                                        <a href="cherokee-language.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Learn Cherokee <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tribal-registration.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Register as a Citizen <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tag-office.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Renew My Tag <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>   
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('images/2.jpg');">
                        <div class="opacity-extra-medium bg-extra-dark-gray"></div>
                        <div class="container position-relative full-screen xs-height-400px">
                            <div class="slider-typography" style="text-align: right">
                                <div class="slider-text-middle-main">
                                    <div class="slider-text-middle">
                                        <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 42px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 500;">
                                            Find Your Way 
                                        </span>
                                        <a href="cherokee-language.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Learn Cherokee <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tribal-registration.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Register as a Citizen <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tag-office.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Renew My Tag <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>   
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('images/3.jpg');">
                        <div class="opacity-extra-medium bg-extra-dark-gray"></div>
                        <div class="container position-relative full-screen xs-height-400px">
                            <div class="slider-typography" style="text-align: right">
                                <div class="slider-text-middle-main">
                                    <div class="slider-text-middle">
                                        <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 42px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 500;">
                                            Find Your Way 
                                        </span>
                                        <a href="cherokee-language.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Learn Cherokee <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tribal-registration.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Register as a Citizen <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tag-office.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Renew My Tag <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>   
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination swiper-pagination-white swiper-full-screen-pagination"></div>
                <div class="swiper-button-next swiper-button-black-highlight display-none"></div>
                <div class="swiper-button-prev swiper-button-black-highlight display-none"></div>
            </div>
        </section>
        <!-- end slider section -->

        <section class="wow fadeIn" style="border-top: 5px solid #99383b; visibility: visible; animation-name: fadeIn;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center-col margin-three-bottom sm-margin-40px-bottom xs-margin-30px-bottom last-paragraph-no-margin">
                        <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Osiyo! </h5>
                        <p class="margin-15px-bottom" style="font-size: 1.2em; color: #99383b;">
                            THOSE IN SEARCH OF THE TRUTH!
                        </p>

                        <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                        Welcome to all who are in search of their aboriginal heritage and culture!  As the original people of this land called turtle island, we have experienced being called many titles. Our foreign visitors rarely asked us as Tsalagi (pronounced Cherokee) what we call ourselves; however, AnIyunwiya best describes the Tsalagi people as, ‘the principle people as a people of another language!
                        </p>
                        <br>
                        <p class="margin-15px-bottom" style="font-size: 1.2em;color: #585d65;">
                        From the inception of the Spanish and Portuguese influence all people of color despite our location, they were called, “the Indios people” or “Indian” since the mid 1400’s! prior to that we, as the aboriginal people or the original people of Elohi (earth), maintained our tribal names which were descriptive of where ever we experienced other aboriginals or our European counterparts.
                        </p>

                        <a href="#"> 
                            <p class="margin-15px-bottom" style="font-size: 1.2em; color: #99383b;"> 
                                Read More
                            </p>
                        </a>
                        <br>
                        <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> MISSION OBJECTIVE: TO IMPROVING THE QUALITY OF LIFE ON ELOHI (EARTH) </h6>
                        <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                            As a Sovereign Nation, our mission objective is generally designed to reverse the destructive trends that has g protection; law enforcement; constructive judicial and penal management; innovations in agriculture! our overall projections are influenced by the overall concept of developing effective guidelines, protocols and laws aimed at improving the quality of life on earth! Our first project will appear in the form of an economic system that will level the playing field for all to achieve their dreams of life, liberty and happiness! 
                        </p>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 xs-text-center" style="background-color: #fff3cd;border: 2px solid #bfba2e;padding: 20px;">

                        <figure class="wp-caption alignright"><img alt="" src="images/covid-19.jpg" data-no-retina="">
                            <figcaption class="wp-caption-text text-uppercase">Coronavirus (COVID-19)</figcaption>
                        </figure>
                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;">
                                Coronavirus (COVID-19)
                            </h6>

                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                Our Cherokee word for respect is ᎤᏬᎯᏳᎯ, and we are upholding these values by helping prevent the spread of COVID-19, as best we can.
                                The Cherokee Nation is taking steps to keep our community safe!
                            </p>
                            <br/>
                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                “We must all act to stay healthy, monitoring our own health and that of our family, limiting travel, 
                                if possible, and checking on our elders who are most vulnerable,” Principal Chief Chuck Hoskin Jr. said. 
                                “The Cherokee Nation has secured federal funding to help us combat and treat any coronavirus cases, ensure our 
                                medical staff are trained to identify, treat and respond to patients efficiently and have the ability to do our own testing.”
                            </p>
                            <br/>
                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                A call center (1-833-528-0063) has been established for tribal citizens who have questions about COVID-19. 
                                It is open from 8 a.m. to 8 p.m. Monday-Friday. If tribal citizens have symptoms like cough, fever or respiratory 
                                problems, they should contact their Cherokee Nation Health center first, before entering. Local meetings and at-large 
                                community gatherings are also being postponed for the time being to help keep our employees and tribal citizens safe.
                            </p>
                            <br/>
                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65; font-weight: 800">
                                As of May 15th, Cherokee Nation Health Services has 74 confirmed cases of COVID-19.
                            </p>
                            <br/>
                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                For more information, please visit Health Services.
                            </p>
                            <br/>
                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                For continued updates on closings and cancellations, visit Anadisgoi.
                            </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="wow fadeIn xs-margin-80px-top" style="visibility: visible; animation-name: fadeIn;">
            <div class="container">
                <div class="row equalize sm-equalize-auto">
                    <div class="col-md-4 col-sm-12 col-xs-12 display-table wow fadeInRight last-paragraph-no-margin" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInRight; height: 402px;">
                        <div class="display-table-cell vertical-align-middle" style="position: relative;">
                            <a href="executive-branch.php" alt="">
                                <img src="images/a.PNG" alt="" class="width-100" style="height: 240px">
                                <div class="bottom-left">Meet Chief and Deputy</div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center display-table sm-margin-50px-bottom wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft; height: 402px;">
                        <div class="display-table-cell vertical-align-middle" style="position: relative;">
                        <a href="executive-branch.php" alt="">
                            <img src="images/logo.PNG" alt="" class="width-100" style="height: 240px">
                            <div class="bottom-left">PUBLIC SAFETY - TSALAGI POLICE DEPARTMENT AND THE TSALAGI MARSHALS SERVICE</div>
                        </a>
                        </div>
                    </div>
                   
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center display-table sm-margin-50px-bottom wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft; height: 402px;">
                        <div class="display-table-cell vertical-align-middle" style="position: relative;">
                            <a href="#" alt="">
                                <img src="images/c.jpg" alt="" class="width-100" style="height: 240px">
                                <div class="bottom-left">TSALAGI COMMUNITY DEVELOPMENT SERVICES</div>
                            </a>
                        </div>
                    </div>

                    <div style="margin-top: 20px"></div>

                    <div class="col-md-4 col-sm-12 col-xs-12 display-table wow fadeInRight last-paragraph-no-margin" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInRight; height: 402px;">
                        <div class="display-table-cell vertical-align-middle" style="position: relative;">
                            <a href="#" alt="">
                                <img src="images/b.jpg" alt="" class="width-100" style="height: 240px">
                                <div class="bottom-left">TSALAGI COMMERCE SERVICES FOR ECONOMIC AND BUSINESS DEVELOPMENT</div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 display-table wow fadeInRight last-paragraph-no-margin" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInRight; height: 402px;">
                        <div class="display-table-cell vertical-align-middle" style="position: relative;">
                            <a href="#" alt="">
                                <img src="images/e.jpg" alt="" class="width-100" style="height: 240px">
                                <div class="bottom-left">COMMUNITY AND CULTURAL OUTREACH TO ABORIGINAL TRIBAL ASSOCIATIONS</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        
        <section class="wow fadeIn xs-margin-80px-top" style="visibility: visible; animation-name: fadeIn;">
            <div class="container">
                <div class="row equalize sm-equalize-auto">
                    <div class="col-md-4 col-sm-12 col-xs-12 sm-margin-50px-bottom wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft; height: 402px;">
                        <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;">
                            What's Happening
                        </h6>
                        <div class="display-table-cell vertical-align-middle">
                            <img src="images/census-wh.jpg" alt="" class="width-100">
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-12 display-table wow fadeInRight last-paragraph-no-margin" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInRight; height: 402px;">
                        <div class="display-table-cell vertical-align-middle">
                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;">
                                    2020 Census
                            </h6>

                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                The Cherokee Nation is kicking off its #CherokeeNationCounts 2020 Census campaign, urging
                                tribal citizens to complete the Census completely and accurately to help ensure the tribe receives key 
                                funding for programs including Indian Health Service and housing allocations through Housing and Urban Development.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="separator-line-horrizontal-full bg-extra-light-gray margin-five-tb width-100"></div>

                <div class="row equalize sm-equalize-auto">
                    <div class="col-md-8 col-sm-12 col-xs-12 display-table wow fadeInRight last-paragraph-no-margin" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInRight; height: 402px;">
                        <div class="display-table-cell vertical-align-middle">
                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;">
                                Public School Appreciation Day
                            </h6>

                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                The Cherokee Nation contributed more than $6 million to 108 school districts during the tribe’s annual Public School Appreciation Day March 5. 
                                This year’s disbursement is the largest since the tribe began its annual contributions in 2002.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 sm-margin-50px-bottom wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft; height: 402px;">
                        <div class="display-table-cell vertical-align-middle">
                            <img src="images/chiefhoskin.jpg" alt="" class="width-100">
                        </div>
                    </div>
                </div>

                <div class="separator-line-horrizontal-full bg-extra-light-gray margin-five-tb width-100"></div>

                <div class="row equalize sm-equalize-auto">
                    <div class="col-md-4 col-sm-12 col-xs-12 sm-margin-50px-bottom wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft; height: 402px;">
                        <div class="display-table-cell vertical-align-middle">
                            <img src="images/mankiller_groundbreaking.jpg" alt="" class="width-100">
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-12 display-table wow fadeInRight last-paragraph-no-margin" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInRight; height: 402px;">
                        <div class="display-table-cell vertical-align-middle">
                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;">
                                Wilma P. Mankiller Health Center
                            </h6>

                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                Cherokee Nation broke ground March 3 at the Wilma P. Mankiller Health Center for 
                                an 80,000-square-foot expansion to modernize the facility originally built in 1994.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="separator-line-horrizontal-full bg-extra-light-gray margin-five-tb width-100"></div>

            </div>
        </section>

<?php
include_once('footer.php');
?>