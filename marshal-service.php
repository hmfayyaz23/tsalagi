<?php
	include_once('header.php');
?>


        <!-- start slider section  -->
        <section class="wow fadeIn no-padding main-slider mobile-height top-space" >
            <div class="swiper-full-screen swiper-container width-100 white-move">
                <div class="swiper-wrapper">
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('images/1.jpg');">
                        <div class="opacity-extra-medium bg-extra-dark-gray" style="background: linear-gradient(to right, rgba(0,0,0,0.0) 0%,rgba(0,0,0,0.8) 78%,rgba(0,0,0,0.8) 100%)"></div>
                        <div class="container position-relative full-screen xs-height-400px">
                            <div class="slider-typography" style="text-align: right">
                                <div class="slider-text-middle-main">
                                    <div class="slider-text-middle">
                                        <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 42px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 500;">
                                            Find Your Way 
                                        </span>
                                        <a href="cherokee-language.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Learn Cherokee <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tribal-registration.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Register as a Citizen <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tag-office.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Renew My Tag <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>   
                                            </span>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('images/2.jpg');">
                        <div class="opacity-extra-medium bg-extra-dark-gray"></div>
                        <div class="container position-relative full-screen xs-height-400px">
                            <div class="slider-typography" style="text-align: right">
                                <div class="slider-text-middle-main">
                                    <div class="slider-text-middle">
                                        <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 42px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 500;">
                                            Find Your Way 
                                        </span>
                                        <a href="cherokee-language.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Learn Cherokee <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tribal-registration.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Register as a Citizen <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tag-office.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Renew My Tag <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>   
                                            </span>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('images/3.jpg');">
                        <div class="opacity-extra-medium bg-extra-dark-gray"></div>
                        <div class="container position-relative full-screen xs-height-400px">
                            <div class="slider-typography" style="text-align: right">
                                <div class="slider-text-middle-main">
                                    <div class="slider-text-middle">
                                        <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 42px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 500;">
                                            Find Your Way 
                                        </span>
                                        <a href="cherokee-language.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Learn Cherokee <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tribal-registration.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Register as a Citizen <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tag-office.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Renew My Tag <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>   
                                            </span>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination swiper-pagination-white swiper-full-screen-pagination"></div>
                <div class="swiper-button-next swiper-button-black-highlight display-none"></div>
                <div class="swiper-button-prev swiper-button-black-highlight display-none"></div>
            </div>
        </section>
        <!-- end slider section -->

        <section class="wow fadeIn" style="border-top: 5px solid #99383b; visibility: visible; animation-name: fadeIn;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center-col margin-three-bottom sm-margin-40px-bottom xs-margin-30px-bottom last-paragraph-no-margin">
                        <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Marshal Service
                        </h5>
                        <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                            The Cherokee Nation Marshal Service is a certified law enforcement agency with jurisdiction throughout the Cherokee Nation.  The Marshal Service is cross-deputized with 50 municipal, county, state and federal agencies. With more than 32 Deputy Marshals, the agency offers an array of special teams focusing on prevention and justice in matters concerning criminal activities.
                            <br><br>
                            The Marshal Service provides an array of special emphasis units including:
                            <br><br>
                                                > D.A.R.E. <br>
                                                > Dive Team <br>
                                                > Marshal Swift Water Rescue <br>
                                                > Domestic Violence Presentations <br>
                                                > Methamphetamine Presentations <br>
                                                > Narcotics Unit <br>
                                                > Gang Awareness <br>
                                                > Vehicle Accident Investigation <br>
                                                > Special Operations Team <br>
                                                > Training 
                            <br><br>
                            All Cherokee Nation Marshals are required to receive training at the Federal Training Center.  The Artesia Training Division is responsible for designing, developing, coordinating, and administering advanced and specialized training programs for the United States Border Patrol, Bureau of Indian Affairs, Transportation Security Administration, and other partnering organizations.  Basic and advanced training programs are conducted for the Department of Interior's Bureau of Indian Affairs under the auspices of the Indian Police Academy.
                            <br><br>
                            Sex Offender Registry
                            <br><br>
                            Pursuant the Adam Walsh Child Protection and Safety Act of 2006 and the 2008 Cherokee Nation Sex Offender Registration and Notification Act, any convicted sex offender who lives, works, or attends school on “Indian Country” within Cherokee Nation’s 14 County Jurisdiction must register as a Sex Offender with Cherokee Nation, in addition to any other state, territory, or tribal registration.
                        </p>
                    </div>

           
                </div>
            </div>
        </section>

<?php
include_once('footer.php');
?>
