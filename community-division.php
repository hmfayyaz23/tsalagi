<?php
	include_once('header.php');
?>


        <!-- start tab style 04 section -->
        <section class="wow fadeIn padding-six-tb bg-light-gray" style="margin-top: 123px;">
            <div class="container tab-style4">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12 margin-30px-bottom xs-margin-40px-bottom">
                        <div class="position-relative overflow-hidden width-100">
                            <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Community Service Division </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding-right" style="border-right: 1px solid #e5e5e5;">
                        <div class="display-table width-100 height-100">
                            <div class="display-table-cell vertical-align-middle">
                                <!-- start tab navigation -->
                                <ul class="nav nav-tabs alt-font text-uppercase text-small display-inherit font-weight-600">
                                    <li class="active"><a href="#tab-four1" data-toggle="tab"> Community Service Division </a></li>
                                    <li><a href="#tab-four2" data-toggle="tab">Loan Programs</a></li>
                                    <li><a href="#tab-four3" data-toggle="tab">Mortgage Assistance Program </a></li>
                                    <li><a href="#tab-four4" data-toggle="tab">Small Business Assistance Center</a></li> 
                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 no-padding-left">
                        <div class="tab-content" style="border: 0">
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in active" id="tab-four1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Tsalagi Nation Commerce Services is dedicated to helping build the economic security of Tsalagi Nation citizens and communities and offers programs that assist in building financial security, advancing and encouraging small business development and entrepreneurship, promoting artist development and fostering community tourism.
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Specific programs offered through commerce services include the Small Business Assistance Center and a variety of tools to promote financial security like the Mortgage Assistance Program, iSave, foreclosure prevention services, homebuyers education classes, and financial coaching. Commerce services also operates the Kawi Cafe.
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                For more information about Tsalagi Nation Commerce Services, call 918-453-5000. 
                                            </p>
                                            <br>
                                            <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                <p class="no-margin-bottom public-notices">
                                                    Public Notices
                                                </p>
                                                <p class="no-margin-bottom downloadPDF" >
                                                    <a href="https://www.Tsalagi.org/media/pc4h3p4p/financial-relief-through-cares-act-2020-4-6.pdf" style="border-bottom: 1px solid blue;"> Financial Relief through CARES Act 2020 </a> <br/>
                                                    <span style="font-size: 14px; font-style: italic;">98.6 KB -- Created:4/8/2020  |  Updated:4/8/2020</span>
                                                    <br><br>
                                                    <span>Consumer Financial Relief information regarding the CARES Act of 2020.</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four2">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Loan Programs </h6>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;"> LOAN TERMS as of March 16, 2020  </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1em;color: #585d65;"> Prime Currently 3.25% </p>
                                            <br>
                                            <table>
                                                    <tr>
                                                      <th></th>
                                                      <th>Loan Size</th>
                                                      <th>Loan Term*</th>
                                                      <th>Collateral</th>
                                                      <th>Interest Rate**</th>
                                                      <th>Closing Fees</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Micro</th>
                                                        <td>$100 to $25,000 </td>
                                                        <td>Up to 15 years </td>
                                                        <td>Equipment or other property purchased with proceeds of the loan, or personal assets owned by the applicant</td>
                                                        <td>Prime, fixed </td>
                                                        <td>2% of loan </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Business</th>
                                                        <td>$100 to $500,000</td>
                                                        <td>Up to 15 years </td>
                                                        <td>Real or personal property; Equipment or other property purchased with proceeds of the loan, or personal assets owned by the applicant</td>
                                                        <td>Prime + 1-3% depending on loan rating*** at time of closing; 1-3 yrs fixed thereafter reset annually </td>
                                                        <td>2% of loan </td>
                                                    </tr>
                                                    <tr>
                                                        <th>IRP</th>
                                                        <td>$100 to $250,000 </td>
                                                        <td>Up to 15 years </td>
                                                        <td>Equipment or other property purchased with proceeds of the loan, or personal assets owned by the applicant</td>
                                                        <td>Prime, fixed </td>
                                                        <td>2% of loan </td>
                                                    </tr>
                                                
                                            </table>

                                            <br>    
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">** Loan term based on the conservative remaining life of collateral and cash flow.</p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1em;color: #585d65;"> ** All interest rates are based upon the New York Prime Lending Rate, as stated in the Wall Street Journal on the date of closing. </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1em;color: #585d65;"> 
                                                *** Loan rating is contingent upon the client’s collateral coverage, credit history, cash flow stability, equity investment, and business qualifications.
                                            </p>

                                            <br>
                                            <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                <p class="no-margin-bottom public-notices">
                                                        Downloads
                                                </p>
                                                <p class="no-margin-bottom downloadPDF" >
                                                    <a href="https://www.Tsalagi.org/media/4cga4a3n/commerical-business-loan-supplemental.pdf" style="border-bottom: 1px solid blue;"> Commercial Business Loan Supplemental </a> <br/>
                                                    <span style="font-size: 14px; font-style: italic;">305.4 KB -- Updated:5/29/2019 </span>
                                                    <br><br>
                                                    <span>This document contains tools for developing your own personalized business plan.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four3">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Mortgage Assistance Program
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Mortgage Assistance and Self-sufficiency programs allow Tsalagi Nation citizens to build and repair their credit and reach personal financial goals. This program provides qualified Tsalagi Nation citizens with down payment and closing cost assistance when obtaining a mortgage to purchase or construct a home.  Income guidelines are 80% of National Median Income (NMI) or below.
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The mortgage assistance program (MAP) provides qualified Tsalagi Nation citizens with down payment assistance up to $20,000. For more information, please contact Tsalagi Nation Commerce Services at 918-453-5536 or sbac@Tsalagi.org.
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The head of household or spouse must be a member of a federally recognized tribe. Preference is given to Tsalagi Nation citizens.
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Applicants must be first-time home buyers; exceptions will be made for individuals who lost their home due to documented domestic violence and for individuals who have only owned a mobile residence. Applicants must be willing to reside within the Tsalagi Nation's jurisdictional boundaries and must obtain a loan through an approved lender, preferably a Section 184 or USDA loan. The loan must be approved by the Tsalagi Nation. Program participants must own and reside in the home for a specified period of time or a prorated payback will apply. An onsite environmental review and inspection will be conducted and home buyer’s education classes are required. Purchase size is limited to five (5) acres and the purchase price may not exceed $150,000.
                                            </p>
                                            <br>
                                            <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                <p class="no-margin-bottom public-notices">
                                                        Downloads
                                                </p>
                                                <p class="no-margin-bottom downloadPDF" >
                                                    <a href="https://www.Tsalagi.org/media/4cga4a3n/commerical-business-loan-supplemental.pdf" style="border-bottom: 1px solid blue;"> Mortgage Assistance Program (MAP) Application </a> <br/>
                                                    <span style="font-size: 14px; font-style: italic;">495.4 KB -- Updated:2/13/2020 </span>
                                                    <br><br>
                                                    <span>The iSave Program is designed to help working Native Americans save money to achieve their financial goals.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four4">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Small Business Assistance Center
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Tsalagi Nation Small Business Assistance Center (SBAC) is proud to serve and support Indian-owned businesses by providing access to capital for new business start-ups and business expansion projects. SBAC is dedicated to the growth of Indian-owned companies, and also provides business coaching services designed to enhance the operation of small businesses. Programs offered through the Tsalagi Nation SBAC are designed to encourage Native American-owned companies to have a greater presence in the business community.
                                            </p>

                                           <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                For more information on the Tsalagi Nation Small Business Assistance Center, call 918-453-5000. 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four5">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Surgical Technology Program
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Surge Tech Program prepares competent entry-level surgical technologists in the cognitive, skills and behavior learning domains. A surgical technologist is an integral member of the surgical team, responsible for preparation of sterile supplies, equipment and instruments, and assisting the surgeon in their use. The surgical technologist most frequently serves as instrument handler, setting up instruments and passing them to the surgeon. A surgical technologist may also assist the surgeon by performing tasks such as retracting incisions, cutting sutures, and utilizing suction to maintain a clear field of vision for the surgeon.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Tsalagi Nation Surgical Technology Program is accredited through the Commission on Accreditation of Allied Health Education Programs (CAAHEP).  
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Accreditation exists to establish, maintain and promote appropriate standards of quality for educational programs. These standards are used for the development, evaluation, and self-analysis of the surgical technology program. Accreditation in the health-related disciplines also serves a very important public interest. Along with certification, accreditation is a tool intended to help assure a well-prepared and qualified workforce providing health care services. Students who graduate from an accredited program are eligible to take the certification examination. 
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four6">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Talking Leaves Job Corps
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Talking Leaves Job Corps Center opened its doors on October 18, 1978. The center was formerly housed at Northeastern State University’s Loeser Hall. As the enrollment grew and the need for dorm space increased, TLJC moved to the Tsalagi Nation in 1988 and later onto their current facility in 1994. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Today, TLJC enjoys a new campus currently serving 197 students and recently received a new five-year contract to continue serving students and assist them in becoming employable for the workforce and equipped for higher education.  
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                For more than four decades, Job Corps has made a difference in the lives of more than 2.6 million economically disadvantaged young Americans. Through a willingness to embrace and welcome change, this voluntary education and job training program continues to offer innovative career technical, academic and social skills training to students at 125 centers nationwide.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TLJC has open enrollment and is currently taking applications. For more information or to apply call 918-456-9959.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four7">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                TERO
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Tribal Employment Rights Office monitors and enforces the Tsalagi Nation Tribal TERO Ordinance to ensure employment rights are protected.  Enforcement of employment rights is funded through the Equal Employment Opportunity Commission (E.E.O.C.)  The TERO office will investigate and mediate employment discrimination complaints involving unfair employment procedures or practices prohibited by Title VII, ADA, ADEA, & EPA.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The TERO office also negotiates for job vacancies with contractors doing business with the Tsalagi Nation and refers qualified Native American workers to fill those vacancies.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TERO also maintains a list of Native-owned businesses. This list is used by CNE, CNI and the Tsalagi Nation Purchasing Department when letting contracts out for bid.  The TERO Ordinance allows for TERO Certified firms to receive preferential treatment in the bid process.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Applicants wishing to apply as a TERO vendor can apply with the TERO office, where the application and all requested documentation will be reviewed. Once the application is complete a site visit will be conducted, and then the applicant will be invited to meet with the TERO committee which decides to certify or not certify a company. If certified, the vendor name will be placed on the TERO vendor list.  If denied certification, vendors many not reapply for a period of one year.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->

                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four8">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Vocational Education and Training
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training, also known as Vocational Education and Training and Career and Technical Education, provides job-specific technical training for work in the trades. These programs generally focus on providing students with hands-on instruction and can lead to certification, a diploma or certificate.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training can also give applicants an edge in job searches since they already have the certifiable knowledge they need to enter the field.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four9">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Vocational Rehabilitation Program
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    The Tsalagi Nation Vocational Rehabilitation (CNVR) Program began in 1992 as the first continuous Tribal Vocational Rehabilitation program in the state of Oklahoma. It is a discretionary grant with a five-year grant cycle. Vocational Rehabilitation's fiscal year is from October 1 to September 30. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    CNVR is not a scholarship program but is based on eligibility. If a person is determined eligible, CNVR is able to assist with the cost of training/re-training at universities, colleges, and technology centers to acquire the skills and education needed to become gainfully employed or retain gainful employment.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four10">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Contact Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <div class="widget-title alt-font text-extra-small text-uppercase margin-15px-bottom font-weight-600">contact information</div>
                                                <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                                                <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                                                <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                    </div>       
                </div>                                
            </div>
        </section>
        <!-- end tab style 04 section -->

<?php
include_once('footer.php');
?>
