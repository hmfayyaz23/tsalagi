<?php
	include_once('header.php');
?>


        <!-- start tab style 04 section -->
        <section class="wow fadeIn padding-six-tb bg-light-gray" style="margin-top: 123px;">
            <div class="container tab-style4">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12 margin-30px-bottom xs-margin-40px-bottom">
                        <div class="position-relative overflow-hidden width-100">
                            <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Police Department

                                </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding-right" style="border-right: 1px solid #e5e5e5;">
                        <div class="display-table width-100 height-100">
                            <div class="display-table-cell vertical-align-middle">
                                <!-- start tab navigation -->
                                <ul class="nav nav-tabs alt-font text-uppercase text-small display-inherit font-weight-600">
                                    <li class="active"><a href="#tab-four1" data-toggle="tab">Police Department

                                        </a></li>
                                    <li><a href="#tab-four2" data-toggle="tab">Clinical Care</a></li>
                                    <li><a href="#tab-four3" data-toggle="tab">Corona Virus (COVID-19) </a></li>
                                    <li><a href="#tab-four4" data-toggle="tab">Public Health</a></li>
                                    <li><a href="#tab-four5" data-toggle="tab">Patient Portal</a></li>
                                    <li><a href="#tab-four6" data-toggle="tab">Notice of Health Information Practices</a></li>
                                    <li><a href="#tab-four7" data-toggle="tab">Contact Us</a></li>
                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 no-padding-left">
                        <div class="tab-content" style="border: 0">
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in active" id="tab-four1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Tsalagi Nation Police Department is the largest tribally-operated health system in the country and offers a variety of services to Tsalagi Nation citizens, as well as citizens of other federally recognized tribes. Through the health system, the tribe operates eight health centers and W.W. Hastings Hospital within the tribe’s 14-county jurisdiction. On the Hastings campus a 469,000 square foot outpatient health facility will also open this fall.
                                                <br><br>
                                                Health Services include:
                                                <br><br>
                                                Chronic/Acute Care <br>
                                                Inpatient Hospital <br>
                                                Pediatric Care <br>
                                                Podiatry <br>
                                                Nursing <br>
                                                Radiology <br>
                                                Laboratory <br>
                                                Physical Therapy <br>
                                                Pharmacy <br>
                                                Dental <br>
                                                Nutrition <br>
                                                Optometry <br>
                                                Contract Health <br>
                                                Diabetes Prevention Program <br>
                                                Cancer Prevention Program <br>
                                                Women, Infants, Children (WIC) <br>
                                                Behavioral Health <br>
                                                Public Health Nursing <br>
                                                Emergency Medical Services <br>
                                                Residential Adolescent Treatment Center <br>
                                                Public Health (Physical Activity, Healthy Eating, & Tobacco Free) <br>
                                         </p>
                                         <br>
                                         <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                             <p class="no-margin-bottom public-notices">
                                                Public Notices
                                             </p>
                                            
                                            
                                             <p class="no-margin-bottom downloadPDF" >
                                                 <a href="https://health.Tsalagi.org/media/btvbhv0u/20200326-governing-board-agenda-revised-final.pdf" style="border-bottom: 1px solid blue;">
                                                    3-26-20 Governing Board Agenda
                                                 </a> <br/>
                                                 <span style="font-size: 14px; font-style: italic;">  290 KB -- Created:3/17/2020  |  Updated:3/17/2020</span>
                                                 <br><br>
                                                 <span>                                             Agenda for Governing Board Meeting March 26, 2020 at 1:00 pm at CNOHC Administration Board Room, 2nd Floor
                                                </span>
                                             </p>
                                         </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four2">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Clinical Care

                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Clinical care includes outpatient and inpatient services and access to primary care physicians throughout the Tsalagi Nation.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four3">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Corona Virus (COVID-19)

                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                As the situation with Coronavirus (COVID-19) evolves, Tsalagi Nation Health understands the public concern for a potential outbreak in our area.
                                                Health Services is actively working with our partners at the federal and state level to assure we are ready to serve the needs of our patients, families, and communities. 
                                                Across our health system, our hospital and health centers are focused on preparing physicians to identify, isolate, and care for patients known or suspected of having COVID-19. A leadership team comprised of physicians, nurses, infection prevention, and other subject matter experts meets daily to review the latest guidance and develop plans to implement the most updated recommendations.
                                                <br><br>
                                                Our patients and communities should know that Tsalagi Nation Health Services regularly participates in preparedness drills so that we are equipped to handle the unexpected. Our physicians and staff routinely receive training on how to best protect themselves and our patients.
                                                <br><br>
                                                It is important to remember that health systems frequently confront disease outbreaks, adapting quickly to serve their communities. We have overcome these challenges in the past and stand ready to undertake this most recent challenge. Tsalagi Nation Health Services wants to be a source of the most reliable and current information available to keep our patients and communities educated with information they can trust to best protect themselves and their families.
                                            </p>
                                            <br/>

                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four4">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Public Health
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Tsalagi Nation Public Health promotes and protects the health of Tsalagi Nation citizens and the communities where they live, learn, work and play. Tsalagi Nation Public Health programs strive to promote healthy behavior and lifestyle. Programs range from the Male Seminary Recreation Center, WINGS program, WIC, community partnerships, walking groups, Cancer Programs and more.
                                                <br><br>
                                                For more information about Public Health please visit TsalagiPublicHealth.org
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four5">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Patient Portal

                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Tsalagi Nation is dedicated to helping improve your overall health care experience by providing convenient, streamlined resources to help you better manage your health. We now offer the ability for you to securely connect some of the health management apps you may use (i.e. fitness trackers, dietary trackers, etc.) to your health record.
                                                <br><br>
                                                If you need help with your login or are interested in this opportunity, let us know by contacting the help desk at 877-621-8014. State that you would like to add an app to your health record.
                                                <br><br>
                                                Patients can view their patient portal account at the Online Patient Portal.
                                                <br><br>
                                                New users can enroll by following the Patient Portal Registration Instructions.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four6">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Notice of Health Information Practices
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Tsalagi NATION
                                                <br><br>
                                                NOTICE OF HEALTH INFORMATION PRACTICES
                                                <br><br>
                                                Effective Date:  January 15, 2018                              
                                                <br><br>
                                                This notice describes how medical information about you may be used and disclosed and how you can get access to this information.  Please review it carefully.
                                                <br><br>
                                                PURPOSE: This Notice of Health Information Practices describes how we may use and disclose your Protected Health Information (PHI) to carry out treatment, payment, or healthcare operations and for other purposes permitted or required by law.  “Protected Health Information” is information that may identify you and that relates to your past, present, or future physical or mental health, and may include your name, address, phone numbers, and other identifying information.  That information is accessible to Tsalagi Nation Health Services staff.
                                                <br><br>
                                                We understand that medical information about you and your health is personal and confidential, and we are committed to protecting the confidentiality of your PHI.  We are required by law to protect your privacy and the confidentiality of your PHI.   Proper safeguards are in place to discourage improper uses or access. 
                                                <br><br>
                                                You will be asked to sign an acknowledgement when you come to a Tsalagi Nation facility or program.  Our purpose is to make you aware of the possible uses and disclosures of your PHI and your privacy rights.  Tsalagi Nation Health Services will care for you even if you refuse to sign the acknowledgement.  Even if you REFUSE to sign the acknowledgement, we WILL use and disclose PHI as outlined in this notice.
                                                <br><br>
                                                As permitted by law, we reserve the right to amend or modify our privacy policies and practices.  These changes in our policies and practices may be required by changes in law or regulations.   Upon request, we will provide you with the most recently revised Notice at any time.  The revised policies and practices will be applied to all PHI we maintain.

                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four-1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                TERO
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Tribal Employment Rights Office monitors and enforces the Tsalagi Nation Tribal TERO Ordinance to ensure employment rights are protected.  Enforcement of employment rights is funded through the Equal Employment Opportunity Commission (E.E.O.C.)  The TERO office will investigate and mediate employment discrimination complaints involving unfair employment procedures or practices prohibited by Title VII, ADA, ADEA, & EPA.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The TERO office also negotiates for job vacancies with contractors doing business with the Tsalagi Nation and refers qualified Native American workers to fill those vacancies.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TERO also maintains a list of Native-owned businesses. This list is used by CNE, CNI and the Tsalagi Nation Purchasing Department when letting contracts out for bid.  The TERO Ordinance allows for TERO Certified firms to receive preferential treatment in the bid process.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Applicants wishing to apply as a TERO vendor can apply with the TERO office, where the application and all requested documentation will be reviewed. Once the application is complete a site visit will be conducted, and then the applicant will be invited to meet with the TERO committee which decides to certify or not certify a company. If certified, the vendor name will be placed on the TERO vendor list.  If denied certification, vendors many not reapply for a period of one year.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->

                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four8">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Vocational Education and Training
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training, also known as Vocational Education and Training and Career and Technical Education, provides job-specific technical training for work in the trades. These programs generally focus on providing students with hands-on instruction and can lead to certification, a diploma or certificate.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training can also give applicants an edge in job searches since they already have the certifiable knowledge they need to enter the field.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four9">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Vocational Rehabilitation Program
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    The Tsalagi Nation Vocational Rehabilitation (CNVR) Program began in 1992 as the first continuous Tribal Vocational Rehabilitation program in the state of Oklahoma. It is a discretionary grant with a five-year grant cycle. Vocational Rehabilitation's fiscal year is from October 1 to September 30. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    CNVR is not a scholarship program but is based on eligibility. If a person is determined eligible, CNVR is able to assist with the cost of training/re-training at universities, colleges, and technology centers to acquire the skills and education needed to become gainfully employed or retain gainful employment.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four7">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Contact Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <div class="widget-title alt-font text-extra-small text-uppercase margin-15px-bottom font-weight-600">contact information</div>
                                                <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                                                <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                                                <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                    </div>       
                </div>                                
            </div>
        </section>
        <!-- end tab style 04 section -->

<?php
include_once('footer.php');
?>
