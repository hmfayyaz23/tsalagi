<?php
	include_once('header.php');
?>

        <!-- start tab style 04 section -->
        <section class="wow fadeIn padding-six-tb bg-light-gray" style="margin-top: 123px;">
            <div class="container tab-style4">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12 margin-30px-bottom xs-margin-40px-bottom">
                        <div class="position-relative overflow-hidden width-100">
                            <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Division of tribal communication  </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding-right" style="border-right: 1px solid #e5e5e5;">
                        <div class="display-table width-100 height-100">
                            <div class="display-table-cell vertical-align-middle">
                                <!-- start tab navigation -->
                                <ul class="nav nav-tabs alt-font text-uppercase text-small display-inherit font-weight-600">
                                    <li class="active"><a href="#tab-four1" data-toggle="tab">Division of tribal communication </a></li>
                                    <li><a href="#tab-four2" data-toggle="tab">Court Information</a></li>
                                    <li><a href="#tab-four3" data-toggle="tab">Downloads  </a></li>
                                    <li><a href="#tab-four4" data-toggle="tab">For Employers</a></li>
                                    <li><a href="#tab-four5" data-toggle="tab">Frequently Asked Questions</a></li>
                                    <li><a href="#tab-four6" data-toggle="tab">Links</a></li>
                                    <li><a href="#tab-four7" data-toggle="tab">Payment Information</a></li>
                                    <li><a href="#tab-four8" data-toggle="tab">Tip Line</a></li>
                                    <li><a href="#tab-four9" data-toggle="tab">Why Choose Us</a></li>
                                    <li><a href="#tab-four10" data-toggle="tab">Contact Us</a></li>
                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 no-padding-left">
                        <div class="tab-content" style="border: 0">
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in active" id="tab-four1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Dear Tsalagi Nation Division of tribal communication Families,
                                                <br><br>
                                                Like most of you, I have been anxiously following the news around COVID-19, wondering what I can do to help in this uncertain and unprecedented time. As a mom, a daughter caring for an elderly father, and a Tsalagi Citizen, I feel more protective of my Family now than ever – which includes all of you.
                                                <br><br>
                                                Supporting our communities has always been a key pillar at the Tsalagi Nation Office of Child Support Services. As such, we are finalizing plans and will be quickly announcing more details and ways to support broader efforts impacting our communities.
                                                <br><br>
                                                I have always believed that we are strongest when we stand together. If you are experiencing financial hardship due to COVID-19 ripple effects, please know that we have options to assist you. Please call our office at 918-453-5444 or Toll-Free at 866-247-5346. Because this situation is complex and ever-changing, our plans may change too so please continue to check our website for resources and updates.
                                                <br><br>
                                                We are always here for you and hope that you stay safe and healthy.
                                                <br><br>
                                                Sincerely,
                                                <br><br>
                                                Kara Pasqua<br>
                                                Director
                                            </p>
                                            <div class="separator-line-horrizontal-full bg-extra-light-gray margin-60px-tb height-2px width-80 center-col"></div>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Tsalagi Nation Office of Child Support Services (CNOCSS) provides genetic testing for paternity establishment; location services, establishes child support obligations and can offer support in modifying child support orders. In addition, CNOCSS has a payment processing unit that ensures recordkeeping for all payment history and a case management unit that ensures regular child support payments. The Child Support Services office focuses on ensuring that parents are supporting their children and acting in the children's best interest and understands that support can come in a myriad of ways. CNOCSS works will all parties in a neutral environment to establish child support that is beneficial to each particular child and family.
                                                <br><br>
                                                Our Mission
                                                <br><br>
                                                The mission of the Tsalagi Nation Office of Child Support Services is to partner with families and other programs to ensure the collaboration of services and resources are in place to build a stable environment for our children that will instill self-sufficiency and wellbeing.
                                                <br><br>
                                                Our goal is to strengthen Tsalagi children, families and communities by ensuring a respectful, friendly environment where families can work together for the benefit of their children.
                                                <br><br>
                                                Who We Serve
                                                <br><br>
                                                Tsalagi children under the age of majority living within the boundaries of the Tsalagi Nation are eligible to receive child support services. A Tsalagi child under the age of majority is defined as a person under the age of 20 years old, who have not graduated from high school and is regularly attending public or private school.
                                                <br><br>
                                                We can generally assist any Native American family if they are willing to submit to Tsalagi Nation tribal court jurisdiction and reside within our 14-county jurisdictional service area. The Tsalagi Nation Office of Child Support Services will serve the full county in cases where the jurisdictional service area takes in only a portion of the county.
                                            </p>
                                            <br>
                                            <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                <p class="no-margin-bottom public-notices">
                                                    Public Notices
                                                </p>

                                                <p class="no-margin-bottom downloadPDF" >
                                                    <a href="https://childsupport.Tsalagi.org/media/vdaadfco/covid-19-ncp-online.pdf" style="border-bottom: 1px solid blue;">
                                                        Letter for Non-Custodial parent experiencing financial hardship
                                                    </a> <br/>
                                                    <span style="font-size: 14px; font-style: italic;"> 54.1 KB -- Created:3/20/2020  |  Updated:3/20/2020</span>
                                                    <br><br>
                                                    <span>To be completed by Non-Custodial parent and returned to caseworker if experiencing financial hardship to COVID-19.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four2">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Court Information </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Child support orders are commonly created after a legal separation or divorce order is completed.  They can also be utilized when an unmarried parent seeks child support either on their own through or a private attorney or if they wish to utilize a child support agency to establish an order. When two parents get divorced, or when an unmarried couple separate, they are faced with some very important and complex issues. One of the most important matters is child support or payments that go towards maintaining a child’s quality of life.
                                                <br><br>
                                                Creating a child support agreement can be difficult, especially if the parties involved are not on good terms. But, it can be done with the assistance of a neutral party to all involved; Tsalagi Nation Office of Child Support Services (CNOCSS) can assist by serving as the neutral party. The first step in determining child support is locating the other parent. This is essential, as the court cannot issue or enforce an order without knowing the whereabouts of both parents.
                                                <br><br>
                                                Next, a judge will examine a number of factors that must be considered when trying to establish an order that all parties can agree upon.  While these elements vary, the goal of a this agreement is to serve the child’s best interests. More specifically, the purpose of a child support agreement is to set payment amounts that will maintain the child’s current standard of living. Factors for consideration include, but are not limited to:
                                                <br><br>
                                                > The monthly and annual income of both parents <br>
                                                > Whether either parent is providing support to a child from a previous relationship <br>
                                                > The amount paid in childcare by the custodial parent, or the parent that has physical custody <br>
                                                > The extent to which each parent should be responsible for paying for the child’s health insurance <br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four3">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Downloads
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                These forms allow parents and families to provide the Tsalagi Nation Office of Child Support Services with additional information so we can better serve you. 
                                            </p>
                                            <br>
                                            <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                <p class="no-margin-bottom public-notices">
                                                    Downloads
                                                </p>
                                                
                                                <p class="no-margin-bottom downloadPDF" >
                                                    <a href="https://childsupport.Tsalagi.org/media/hrip5dgv/address-of-record.pdf" style="border-bottom: 1px solid blue;">
                                                        Address of Record
                                                    </a> <br/>
                                                    <span style="font-size: 14px; font-style: italic;"> 174.6 KB -- Updated:5/7/2019</span>
                                                    <br><br>
                                                    <span>This form is used to designate an Address of Record for service of legal documents.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four4">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                For Employers
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;font-weight: 600">
                                                What is employment verification?
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                What?
                                                <br><br>
                                                > A request to verify an employee’s employment status, wages, and benefits. <br>
                                                > Verification requests from child support also assist in locating non-custodial parents. <br><br>

                                                Why? <br><br>
                                                > Child support agencies need specific and current information to establish or modify orders. <br>
                                                > You must respond promptly and as fully as possible when you receive a verification request. <br><br>
                                                How? <br><br>
                                                > Complete information requested in letters, subpoenas, state-issued forms, or attorney requests <br>
                                            </p>

                                            <br>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;font-weight: 600">
                                                What is an (IWO) Income Withholding Order
                                            </p>
                                            <br><br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                What?
                                                <br><br>
                                                > An income withholding order is a court-mandated payroll deduction. <br><br>

                                                Why? <br><br>
                                                > If a noncustodial parent has an unpaid child support debt and is your employee, a court or child support agency will send you an Income Withholding for Support (IWO) order. <br><br>

                                                Where? <br><br>
                                                > Tsalagi Nation Child Support Payment Center <br>
                                                P.O. Box 21046 <br>
                                                Tulsa, OK  74121 <br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four5">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Frequently Asked Questions
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Below you will find some answers to common questions we get asked.
                                            </p>
                                            <br/>

                                                           <!-- start accordion -->
                        <div class="panel-group accordion-style1" id="accordion-one">
                            <!-- start accordion item -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion-one" href="#accordion-one-link1" class="collapsed" aria-expanded="false"><div class="panel-title font-weight-500 text-uppercase position-relative padding-20px-right">Will your stimulus payment be affected if you owe child support?<span class="pull-right position-absolute right-0 top-0"><i class="ti-plus"></i></span></div></a>
                                </div>
                                <div id="accordion-one-link1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            The Treasury Department provides frequently asked questions on economic impact payments and the Treasury Offset Program. For more information please visit their website at https://fiscal.treasury.gov/top/faqs-for-the-public-covid-19.html
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- end accordion item -->
   
                            <!-- start accordion item -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion-one" href="#accordion-one-link4" class="collapsed" aria-expanded="false"><div class="panel-title font-weight-500 text-uppercase position-relative padding-20px-right">What is the purpose of child support?<span class="pull-right position-absolute right-0 top-0"><i class="ti-plus"></i></span></div></a>
                                </div>
                                <div id="accordion-one-link4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            Child support is based upon the concept that children are entitled to the support of both parents. This recognizes that there are costs associated with raising children. For example, the custodial parent often has to secure larger living quarters than he/she would otherwise need. The custodial parent may need larger or different transportation, in addition to basic food, clothing, school supplies, medical expenses and so forth. The custodial parent is often paying these things directly. Child support is designed to partially offset these expenses and even out the burden based upon the parent's respective incomes.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- end accordion item -->
                            <!-- start accordion item -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion-one" href="#accordion-one-link5" class="collapsed" aria-expanded="false"><div class="panel-title font-weight-500 text-uppercase position-relative padding-20px-right">Why is there a child support program?<span class="pull-right position-absolute right-0 top-0"><i class="ti-plus"></i></span></div></a>
                                </div>
                                <div id="accordion-one-link5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            CN OCSS was established in July 2007 and administers the program with in-house staff and through.
                                            <br><br>
                                            In December 1974, the U.S. Congress amended Title IV of the Social Security Act by adding Part D – Child Support and Establishment of Paternity. Title IV-D required each state to designate an organization to administer a plan for enforcing child support. Services provided by CSS to parents under this law are called IV-D cases.
                                            <br><br> 
                                            Tsalagi Nation has always valued parent’s inherent right to provide for their children.   In December 2004, the Tsalagi Nation tribal council recognized a need to protect Tsalagi sovereignty, customs, and family values by approving a resolution that allowed for the application of funding for a IV-D Tsalagi Nation child support program.   The Tsalagi Nation Office of Child Support Services ( CN OCSS) was established in 2007 and administer the program with in-house staff who strives to provide the very best services to all program participants. 
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- end accordion item -->
                        </div>
                        <!-- end accordion -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four6">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Links
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                > Legal Aid of Oklahoma <br>
                                                > DHS child support calculator<br>
                                                > Oklahoma Indian Legal Services<br>
                                                > Oklahoma Works<br>
                                                > OKDHS Child Support<br>
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four7">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Payment Information
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Most parents pay child support by payroll deduction. Employers will receive an income withholding order that alerts them to withhold the support amount from the paycheck of parents who are ordered to pay support and forward it to the Tsalagi Nation Child Support Payment Center. Once the payment center receives the payment, CNOCSS prepares a check and mails it the custodial parent.
                                                <br><br>
                                                If a parent is not able to pay by payroll deduction, the payment can be mailed in or made in person at the Tsalagi Nation Office of Child Support Services. Payments can only be accepted in the form of a money order or cashier’s check, cash is not accepted.
                                                Payments submitted after 2 P.M. will not be processed until the following business day.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->

                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four8">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Tip Line
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Currently, the Tsalagi Nation Office of Child Support Services (CNOCSS) has many cases in which the non-custodial parent is not providing financial support to their children.
                                                <br><br>
                                                In an effort to improve the lives of children, CNOCSS has established an anonymous tip line to help locate non-custodial parents. If you have any information about someone that CNOCSS is looking for, please email the following information to: childsupporttipline@Tsalagi.org
                                                <br><br>
                                                The information that you provide will be verified. Once the information has been verified, CNOCSS can use the information in several ways:
                                                <br><br>
                                                To file a case in court <br>
                                                To serve a non-custodial parent with court documents <br>
                                                To enforce a court order <br>
                                                To provide information on this website, you must know the first and last name of the non-custodial parent in the case. <br>
                                                When providing information, please give as many details as possible. Please be aware that anyone providing information to the tip line will remain anonymous.
                                                <br><br>
                                                Please provide the following information for the non-custodial parent in question:
                                                <br><br>
                                                First name: <br>
                                                Middle name: <br>
                                                Last name: <br>
                                                Alias: <br>
                                                Address: <br>
                                                City: <br>
                                                State: <br>
                                                Zip Code: <br>
                                                Phone ( ) <br>
                                                Date of Birth: (example: MM-DD-YYYY) <br>
                                                Description of vehicle: <br>
                                                <br><br>
                                                Possible employment (name of employer and address):
                                                <br><br>
                                                Any additional information that will assist CNOCSS in locating the Non-custodial parent:
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four9">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Why Choose Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Call Us Today 866-247-5346 or 918-453-5444
                                                <br><br>
                                                We know you have options when it comes to collecting your unpaid child support. When you choose Tsalagi Nation Office of Child Support Services (CN OCSS) you choose to have the best representation possible to collect your unpaid child support.
                                                <br><br>
                                                In 2018, CN OCSS collected $3,510,288.00 in child support payments. By choosing CN OCSS you choose to enforce your rights for free, we are unlike other agencies and do not collect any fees.
                                             </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four10">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Contact Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <div class="widget-title alt-font text-extra-small text-uppercase margin-15px-bottom font-weight-600">contact information</div>
                                                <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                                                <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                                                <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                    </div>       
                </div>                                
            </div>
        </section>
        <!-- end tab style 04 section -->

<?php
include_once('footer.php');
?>
