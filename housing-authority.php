<?php
	include_once('header.php');
?>


        <!-- start slider section  -->
        <section class="wow fadeIn no-padding main-slider mobile-height top-space" >
            <div class="swiper-full-screen swiper-container width-100 white-move">
                <div class="swiper-wrapper">
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('images/1.jpg');">
                        <div class="opacity-extra-medium bg-extra-dark-gray" style="background: linear-gradient(to right, rgba(0,0,0,0.0) 0%,rgba(0,0,0,0.8) 78%,rgba(0,0,0,0.8) 100%)"></div>
                        <div class="container position-relative full-screen xs-height-400px">
                            <div class="slider-typography" style="text-align: right">
                                <div class="slider-text-middle-main">
                                    <div class="slider-text-middle">
                                        <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 42px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 500;">
                                            Find Your Way 
                                        </span>
                                        <a href="cherokee-language.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Learn Cherokee <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tribal-registration.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Register as a Citizen <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tag-office.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Renew My Tag <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>   
                                            </span>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('images/2.jpg');">
                        <div class="opacity-extra-medium bg-extra-dark-gray"></div>
                        <div class="container position-relative full-screen xs-height-400px">
                            <div class="slider-typography" style="text-align: right">
                                <div class="slider-text-middle-main">
                                    <div class="slider-text-middle">
                                        <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 42px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 500;">
                                            Find Your Way 
                                        </span>
                                        <a href="cherokee-language.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Learn Cherokee <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tribal-registration.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Register as a Citizen <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tag-office.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Renew My Tag <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>   
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('images/3.jpg');">
                        <div class="opacity-extra-medium bg-extra-dark-gray"></div>
                        <div class="container position-relative full-screen xs-height-400px">
                            <div class="slider-typography" style="text-align: right">
                                <div class="slider-text-middle-main">
                                    <div class="slider-text-middle">
                                        <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 42px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 500;">
                                            Find Your Way 
                                        </span>
                                        <a href="cherokee-language.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Learn Cherokee <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tribal-registration.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Register as a Citizen <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tag-office.php" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Renew My Tag <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>   
                                            </span>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination swiper-pagination-white swiper-full-screen-pagination"></div>
                <div class="swiper-button-next swiper-button-black-highlight display-none"></div>
                <div class="swiper-button-prev swiper-button-black-highlight display-none"></div>
            </div>
        </section>
        <!-- end slider section -->

        <section class="wow fadeIn" style="border-top: 5px solid #99383b; visibility: visible; animation-name: fadeIn;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center-col margin-three-bottom sm-margin-40px-bottom xs-margin-30px-bottom last-paragraph-no-margin">
                        <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Housing Authority
                        </h5>
                        <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                            The Housing Authority of the Cherokee Nation was created in 1966 to provide safe, quality housing for citizens of the Cherokee Nation and their families.  Today, HACN continues that original mission and provides housing assistance to our citizens and other Natives through a number of programs.
                            <br><br>
                            HACN offers low-income rental housing, rental assistance, college housing, and housing rehabilitation, as well as offering an innovative new home construction program that allows Cherokee families to more easily achieve their goal of home ownership. HACN uses both federal and tribal funding to provide its services.
                            <br><br>
                            After the reestablishment of the Housing Authority of the Cherokee Nation in 2012, the tribe began building new homes for its citizens through the HACN’s New Home Construction Program. Since the completion of the first homes in August 2012, the program has built about 700 new homes for Cherokee families.
                        </p>
                    </div>

           
                </div>
            </div>
        </section>

<?php
include_once('footer.php');
?>
