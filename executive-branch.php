<?php
	include_once('header.php');
?>


        <!-- start slider section  -->
        <section class="wow fadeIn no-padding main-slider mobile-height top-space" >
            <div class="swiper-full-screen swiper-container width-100 white-move">
                <div class="swiper-wrapper">
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('images/1.jpg');">
                        <div class="opacity-extra-medium bg-extra-dark-gray" style="background: linear-gradient(to right, rgba(0,0,0,0.0) 0%,rgba(0,0,0,0.8) 78%,rgba(0,0,0,0.8) 100%)"></div>
                        <div class="container position-relative full-screen xs-height-400px">
                            <div class="slider-typography" style="text-align: right">
                                <div class="slider-text-middle-main">
                                    <div class="slider-text-middle">
                                        <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 42px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 500;">
                                            Find Your Way 
                                        </span>
                                        <a href="Tsalagi-language.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Learn Tsalagi <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tribal-registration.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Register as a Citizen <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tag-office.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Renew My Tag <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>   
                                            </span>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('images/2.jpg');">
                        <div class="opacity-extra-medium bg-extra-dark-gray"></div>
                        <div class="container position-relative full-screen xs-height-400px">
                            <div class="slider-typography" style="text-align: right">
                                <div class="slider-text-middle-main">
                                    <div class="slider-text-middle">
                                        <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 42px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 500;">
                                            Find Your Way 
                                        </span>
                                        <a href="Tsalagi-language.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Learn Tsalagi <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tribal-registration.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Register as a Citizen <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tag-office.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Renew My Tag <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>   
                                            </span>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('images/3.jpg');">
                        <div class="opacity-extra-medium bg-extra-dark-gray"></div>
                        <div class="container position-relative full-screen xs-height-400px">
                            <div class="slider-typography" style="text-align: right">
                                <div class="slider-text-middle-main">
                                    <div class="slider-text-middle">
                                        <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 42px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 500;">
                                            Find Your Way 
                                        </span>
                                        <a href="Tsalagi-language.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Learn Tsalagi <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tribal-registration.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Register as a Citizen <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>  
                                            </span>
                                        </a>
                                        <a href="tag-office.html" >
                                            <span class="text-large text-very-light-gray font-weight-300 width-95 center-col margin-25px-bottom display-block" style="font-size: 28px;color: #FFF; text-shadow: 2px 2px 4px #000000; font-weight: 400;">
                                                Renew My Tag <span style="text-shadow: none; font-weight: 600;color: #99383b;font-size: 30px;"><</span>   
                                            </span>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination swiper-pagination-white swiper-full-screen-pagination"></div>
                <div class="swiper-button-next swiper-button-black-highlight display-none"></div>
                <div class="swiper-button-prev swiper-button-black-highlight display-none"></div>
            </div>
        </section>
        <!-- end slider section -->

        <section class="wow fadeIn" style="border-top: 5px solid #99383b; visibility: visible; animation-name: fadeIn;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center-col margin-three-bottom sm-margin-40px-bottom xs-margin-30px-bottom last-paragraph-no-margin">
                        <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Executive Branch
                        </h5>
                        <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                            The Tsalagi Nation Executive Branch Service (CNEM) team is a part of the Tsalagi Nation Marshal Service and is comprised of representatives from departments throughout the Tsalagi Nation government. The team prepares for and responds to disasters and emergencies that occur within the Tsalagi Nation jurisdiction.
                            <br><br>
                            CNEM maintains the tribe’s Emergency Operations Center which serves as a command center for reporting emergencies and coordinating tribal response activities. The team works with emergency management teams in cities, towns, counties and the State of Oklahoma, as well as the Federal Emergency Management Agency (FEMA) through a vast network of emergency management professionals.
                            <br><br>
                            Emergency Response: During emergencies, CNEM coordinates tribal emergency operations including:
                            <br><br>
                                                > Monitoring potentially severe events <br>
                                                > Evaluating the possibility for securing state, federal disaster assistance <br>
                                                > Managing tribal resources <br>
                                                > Providing emergency public information <br>
                                                > Developing and distributing situation reports <br>
                                                > Conducting preliminary damage assessments <br> <br>

                       </p>
                    </div>

           
                </div>
            </div>
        </section>

<?php
include_once('footer.php');
?>
