<?php
	include_once('header.php');
?>


        <!-- start tab style 04 section -->
        <section class="wow fadeIn padding-six-tb bg-light-gray" style="margin-top: 123px;">
            <div class="container tab-style4">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12 margin-30px-bottom xs-margin-40px-bottom">
                        <div class="position-relative overflow-hidden width-100">
                            <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Division of health and human services </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding-right" style="border-right: 1px solid #e5e5e5;">
                        <div class="display-table width-100 height-100">
                            <div class="display-table-cell vertical-align-middle">
                                <!-- start tab navigation -->
                                <ul class="nav nav-tabs alt-font text-uppercase text-small display-inherit font-weight-600">
                                    <li class="active"><a href="#tab-four1" data-toggle="tab">Division of health and human services</a></li>
                                    <li><a href="#tab-four2" data-toggle="tab">Tsalagi Nation Transit</a></li>
                                    <li><a href="#tab-four3" data-toggle="tab">Department of Transportation</a></li>
                                    <li><a href="#tab-four4" data-toggle="tab">Engineering and Sanitation</a></li> 
                                    <li><a href="#tab-four5" data-toggle="tab">Environmental Health</a></li> 
                                    <li><a href="#tab-four6" data-toggle="tab">Youth and Adult Services </a></li> 
                                    <li><a href="#tab-four7" data-toggle="tab">Tsa-La-Gi Apartments</a></li> 
                                    <li><a href="#tab-four8" data-toggle="tab">Contact Us</a></li> 

                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 no-padding-left">
                        <div class="tab-content" style="border: 0">
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in active" id="tab-four1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                            Division of health and human services offers programs to improve the quality of life for Indian families and communities by assisting with needs like sanitation, transit and transportation, water and environmental while maintaining cultural sensitivity and encouraging citizens to be productive and self-sufficient.                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Service offered include, Adult Resident Services, Community Youth Development, Department of Transportation, Engineering and Sanitation Facilities Construction,  Environmental Health, Transit, and Tsa-la-gi Apartments                                            
                                            </p>

                                            <br>
                                            <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                <p class="no-margin-bottom public-notices">
                                                    Public Notices
                                                </p>
                                                <p class="no-margin-bottom downloadPDF" >
                                                    <a href="https://www.Tsalagi.org/media/oa1bqjva/public-notice-tip.pdf" style="border-bottom: 1px solid blue;"> Transportation Improvement Program (TIP)</a> <br/>
                                                    <span style="font-size: 14px; font-style: italic;">365 KB -- Created:9/5/2019  |  Updated:9/5/2019
                                                        </span>
                                                    <br><br>
                                                    <span>CNDOT is accepting public comments until 9/24/19 on the attached Transportation Improvement Program (TIP)</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four2">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Tsalagi Nation Transit                                                </h6>
                                            <p class="no-margin-bottom" style="font-size: 1.3em;color: #585d65; font-weight: 600"> Purpose </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.3em;color: #585d65;">
                                                The purpose of Tsalagi Nation’s Transit program is to provide safe, reliable, and low-cost transportation for Native Americans to employment, healthcare, places of higher learning, and other vital destinations through commuter-routes and demand response services.
                                            </p>
     
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.3em;color: #585d65; font-weight: 600"> History </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.3em;color: #585d65;">
                                                The Tsalagi Nation Transit program began in 2008 with Ki Bois Area Transit System (KATS) to provide tribal employees and the general public with low-cost transportation to employment sources, also known as a “park-n-ride” commuter-route service, from Stilwell to Tahlequah.  In 2009, Pelivan Transit came on board to begin the Pryor to Catoosa route and, since then, the program has expanded to include demand responsive services and additional commuter-routes through Muskogee County Transit (MCT) and Cimarron Public Transit.
                                                <br><br>
                                                Effective Immediately, the Tsalagi Nation Department of Transportation will discontinue the Salina to Tahlequah Route beginning on November 6, 2019. The last day of services will be November 5, 2019. If you have any questions or concerns, please contact: transit@Tsalagi.org.                                            </p>
                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four3">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Department of Transportation
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Our mission is to improve the quality of life for Native American families and communities by providing safe roads and bridges for better access to housing, schools, healthcare, business, and employment.
                                                <br><br>
                                                Long-range planning consists of an on-going review and analysis of population, housing, social, and economic data to develop strategies for the prioritization of roads and transit systems based on the goals and objectives of the tribe.
                                                <br><br>
                                                The Department of Transportation (DOT) continually implements and supports cooperative road and bridge projects within the reservation of the Tsalagi Nation by coordinating at the county, state, and federal levels as well as community involvement.  These road projects are federally funded by the Federal Highway Administration.  Tribal Transportation Program (TTP) Funding is used for planning, engineering, and construction of TTP projects. The Tsalagi Nation DOT with its group of professionals is able to produce and monitor projects from inception to construction completion. Motor Fuel Tax, Motor Vehicle Tax, and Special Bridge funds are set aside by the Tsalagi Nation Tribal Council for infrastructure improvement projects which do not meet the standards for TTP funding.
                                                <br><br>
                                                The DOT incorporates the foremost level of technical operations from start to finish.  Strict adherence to state and federal standards, specifications, and regulations are applied to each construction project. The operational capacity for the DOT includes the ability to conduct contracting, transportation planning, right-of-way negotiations, surveying, design, environmental clearances, and construction inspections, of which all meet or exceed Federal Specifications.
                                                <br><br>
                                                The DOT is a diversified department encompassing the needs of the community and sensitive to the needs of individuals. Potential projects are based on the percentage of the Native American population and the changing needs of the entire community.
                                                <br><br>
                                                The objectives of our services are to supply safe direct routes to highways and towns and to provide safe reliable transportation for essential daily living.
                                            </p>

                                           <br>
                                            <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                <p class="no-margin-bottom public-notices">
                                                    Downloads
                                                </p>
                                                <p class="no-margin-bottom downloadPDF" >
                                                    <a href="https://www.Tsalagi.org/media/wrfajj1s/road-improvement-request.pdf" style="border-bottom: 1px solid blue;"> Road Improvement Request </a> <br/>
                                                    <span style="font-size: 14px; font-style: italic;">495.4 KB -- Updated:2/13/2020 </span>
                                                    <br><br>
                                                    <span>Application for requesting improvements to a road within the Tsalagi Nation.                                                        </span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four4">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Engineering and Sanitation
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Sanitation Facilities Construction (SFC) Program provides the following services:
                                            </p>

                                           <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65; font-weight: 600">
                                                Water
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                > Well drilling, including pump, pressure system and all appurtenances:<br>
                                                > Connection to community and rural water systems;<br>
                                                > Existing well system renovation and repair.<br>
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65; font-weight: 600">
                                                Wastewater System
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                > Septic system installation includes a septic tank and drain field and alternative disposal systems;<br>
                                                > Connection to community sewer systems.<br>
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65; font-weight: 600">
                                                Engineering Program
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65">
                                                    The Engineering Program provides engineering design and technical assistance for water, wastewater and solid waste projects. Services provided include design and construction of individual and community water and sewer systems, solid waste clean-up activities, plans, specifications, and topographical surveying. Sanitation facilities construction installation inspections are also provided under this program.
                                            </p>
                                            <br>
                                            <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                <p class="no-margin-bottom public-notices">
                                                    Downloads
                                                </p>
                                                
                                                <p class="no-margin-bottom downloadPDF" >
                                                    <a href="https://www.Tsalagi.org/media/k2eff0ve/sfc-application-rev-04-03-2019.pdf" style="border-bottom: 1px solid blue;"> Sanitation Facilities Construction Application</a> <br/>
                                                    <span style="font-size: 14px; font-style: italic;">554.8 KB -- Updated:5/21/2019
                                                        </span>
                                                    <br><br>
                                                    <span>This application and supporting documentation will allow our office to determine the eligibility and type of services able to be provided.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four5">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Environmental Health
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Office of Environmental Health (OEH) ensures that Tribal facilities are operated in a safe and sanitary manner. They also provide Tribal members with services and information needed to lead environmentally healthy lifestyles through inspections and investigations related to food, water, mold, and wastewater.                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                OEH also provides food service workers with training leading to certification and environmental disease prevention training.                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The OEH provides technical support to the Engineering and Sanitation Facilities Construction Program by performing site evaluations, percolation testing, and water sampling and analysis.                                            </p>
                                            <br/>
                                            <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                <p class="no-margin-bottom public-notices">
                                                    Downloads
                                                </p>
                                                
                                                <p class="no-margin-bottom downloadPDF" >
                                                    <a href="https://www.Tsalagi.org/media/k2eff0ve/sfc-application-rev-04-03-2019.pdf" style="border-bottom: 1px solid blue;"> Sanitation Facilities Construction Application</a> <br/>
                                                    <span style="font-size: 14px; font-style: italic;">554.8 KB -- Updated:5/21/2019
                                                        </span>
                                                    <br><br>
                                                    <span>This application and supporting documentation will allow our office to determine the eligibility and type of services able to be provided.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four6">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Youth and Adult Services
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Adult Resident and Community Youth Development Services was established to preserve the Tsalagi Nation culture, language, and art through various community and individual activities and projects. Crafts and traditional activities remind our elders and educate our youth in the Tsalagi Nation traditions. Along with crafts and activities which may include occasional presenters, this program can link the adult residents with other Tsalagi Nation Services as needed.                                            </p>
                                            <br/>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four7">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Tsa-La-Gi Apartments
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Tsa-La-Gi Apartments is a 90-unit apartment community for seniors or disabled residents located in Sallisaw, OK. The property comprises of eighty-eight 1-bedroom and two 2-bedroom units. The units are spacious with full-size appliances. The property pays all utilities and has laundry on-site. Resident activities are offered in the community room. Rent is income-based with rental assistance from U.S. Housing and Urban Development through a project-based Section 8 contract.                                            </p>
                                            <br/>
                                            <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                <p class="no-margin-bottom public-notices">
                                                    Downloads
                                                </p>
                                                
                                                <p class="no-margin-bottom downloadPDF" >
                                                    <a href="https://www.Tsalagi.org/media/d5ob0jov/tsa-la-gi-rental-application-rev-01-2018.pdf" style="border-bottom: 1px solid blue;"> Tsalagi Rental Application</a> <br/>
                                                    <span style="font-size: 14px; font-style: italic;">374.8 KB -- Updated:5/21/2019
                                                        </span>
                                                    <br><br>
                                                    <span>Application for Section 202/8 Housing</span>
                                                </p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->

                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four8">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Contact Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <div class="widget-title alt-font text-extra-small text-uppercase margin-15px-bottom font-weight-600">contact information</div>
                                                <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                                                <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                                                <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                    </div>       
                </div>                                
            </div>
        </section>
        <!-- end tab style 04 section -->

<?php
include_once('footer.php');
?>
