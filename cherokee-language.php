<?php
	include_once('header.php');
?>

        <!-- start tab style 04 section -->
        <section class="wow fadeIn padding-six-tb bg-light-gray" style="margin-top: 123px;">
            <div class="container tab-style4">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12 margin-30px-bottom xs-margin-40px-bottom">
                        <div class="position-relative overflow-hidden width-100">
                            <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Cherokee Language </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding-right" style="border-right: 1px solid #e5e5e5;">
                        <div class="display-table width-100 height-100">
                            <div class="display-table-cell vertical-align-middle">
                                <!-- start tab navigation -->
                                <ul class="nav nav-tabs alt-font text-uppercase text-small display-inherit font-weight-600">
                                    <li class="active"><a href="#tab-four1" data-toggle="tab">Cherokee Language</a></li>
                                    <li><a href="#tab-four2" data-toggle="tab">Cherokee Language Classes</a></li>
                                    <li><a href="#tab-four3" data-toggle="tab">Cherokee Language Translation</a></li>
                                    <li><a href="#tab-four4" data-toggle="tab">Fonts and Keyboards</a></li>
                                    <li><a href="#tab-four5" data-toggle="tab">Learning Materials</a></li>
                                    <li><a href="#tab-four6" data-toggle="tab">Posters</a></li>
                                    <li><a href="#tab-four7" data-toggle="tab">Technology Localization</a></li>
                                    <li><a href="#tab-four8" data-toggle="tab">Language Programs</a></li>
                                    <li><a href="#tab-four9" data-toggle="tab">Word List</a></li>
                                    <li><a href="#tab-four10" data-toggle="tab">Contact Us</a></li>
                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 no-padding-left">
                        <div class="tab-content" style="border: 0">
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in active" id="tab-four1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Cherokee Nation is committed to preserving and growing the Cherokee language in both spoken and written forms.
                                                <br><br>
                                                The Cherokee language is considered a Class IV language in its degree of difficulty. It is spoken fluently by an estimated 2,500 people worldwide today, with several thousand more being considered beginner or proficient speakers.
                                                <br><br>
                                                The Cherokee syllabary is the written form of the language. It is not an alphabet, but instead contains 85 distinct characters that represent the full spectrum of sounds used to speak Cherokee – one character for each discrete syllable.
                                                <br><br>
                                                Cherokee Nation’s Cherokee language programs include a translation office; community and online language classes; the Cherokee Language Master Apprentice Program; and language technology. Together, these programs offer a variety of services including translation of Cherokee documents, the creation of Cherokee language teaching materials, community and employee Cherokee language classes, and the development and support of Cherokee language on digital devices such as smartphones, tablets, and computers.
                                                <br><br>
                                                The goal of these language programs is the perpetuation of Cherokee language in all walks of life, from day-to-day conversation, to ceremony, digital and online platforms such as social media.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four2">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Cherokee Language Classes                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Cherokee Nation offers Cherokee language classes taught in person at sites across Cherokee Nation’s 14-county tribal area. For those not living inside the boundaries, language classes are also offered in an online setting.                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four3">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Cherokee Language Translation
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Cherokee Nation’s Translation Department provides Cherokee language translations for education, health, legal and other non-profit services.
                                                <br><br>
                                                The Translation Department develops and reviews Cherokee language curriculum and offers free translations to the public for educational, non-profit use. Translation requests must not violate Cherokee Nation policy.
                                                <br><br>
                                                Due to the large volume of requests, Cherokee Nation Translation does not accept unsolicited documents such as poetry, scripts, screenplays, and book manuscripts for translation.
                                                <br><br>
                                                The Cherokee Nation Translation Department is not available for the following requests:
                                                <br><br>
                                                Translations intended for tattoos <br>
                                                English names translated into Cherokee <br>
                                                Translations intended for commercial use <br>
                                                Culturally insensitive material <br>
                                                To submit a translation request, download and complete the request form below and email it to language@cherokee.org.
                                           </p>
                                           <br>
                                           

                                           <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                            <p class="no-margin-bottom public-notices">
                                                Public Notices
                                            </p>
                                            <p class="no-margin-bottom downloadPDF" >
                                                <a href="https://language.cherokee.org/media/swynbqca/ed_translation_form.pdf" style="border-bottom: 1px solid blue;">
                                                    Translation Request Form
                                                </a> <br/>
                                                <span style="font-size: 14px; font-style: italic;"> 61.1 KB -- Updated:5/29/2019</span>
                                                <br><br>
                                                <span>Request form for requesting translations of terms and words to the Cherokee language.
                                                </span>
                                            </p>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four4">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Fonts and Keyboards
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                A variety of Cherokee fonts can be downloaded either at no charge or for a minimal fee. Free keyboard installers are also available for Windows and Mac computers. Instructions are included within this section to help guide the installation processes.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four5">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Learning Materials
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                A variety of materials are available to assist in learning the Cherokee language.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four6">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Posters
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Cherokee Nation Language Program has created a variety of educational posters available for download.                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four7">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Technology Localization
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Cherokee Nation has worked with Microsoft, Google and Facebook to ensure their respective products provide the opportunity for a Cherokee experience while using these services. Below are instructions on how to change a device's language settings to Cherokee, along with screenshots to help navigate the Cherokee interface.
                                                <br><br>
                                                Gmail:
                                                <br><br>
                                                In 2012, Cherokee Nation finished translating Google’s Gmail and it became the first email service to be translated into a Native American language. Below are instructions and a screenshot of a new message with the translated terms.
                                                <br><br>
                                                Windows 8 & 10:
                                                <br><br>
                                                In 2012, Cherokee Nation finished localization of Windows 8 and it became the first computer operating system in a Native American language. The partnership with Microsoft continued as Windows 8 updated to Windows 10, and localization work also branched to Office Online and OneDrive.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->

                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four8">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Language Programs
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Cherokee Nation has multiple programs that aim to promote and revitalize the Cherokee language, create new Cherokee words, and assist in the implementation of Cherokee language into technology. Together, these programs help keep the Cherokee language alive.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four9">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Word List
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                This is the English/Cherokee lexicon or word list. We refrain from the use of the word "dictionary" because it does not provide definitions of words; rather, it provides the translation. This lexicon consists of over 7,000 words and will continue to be extended with more Cherokee word listings. The Cherokee Language Consortium also maintains a word list that is available for download.  
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four10">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Contact Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <div class="widget-title alt-font text-extra-small text-uppercase margin-15px-bottom font-weight-600">contact information</div>
                                                <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                                                <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                                                <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                    </div>       
                </div>                                
            </div>
        </section>
        <!-- end tab style 04 section -->

<?php
include_once('footer.php');
?>