<?php
	include_once('header.php');
?>


        <!-- start tab style 04 section -->
        <section class="wow fadeIn padding-six-tb bg-light-gray" style="margin-top: 123px;">
            <div class="container tab-style4">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12 margin-30px-bottom xs-margin-40px-bottom">
                        <div class="position-relative overflow-hidden width-100">
                            <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Tribal Registration
                                </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding-right" style="border-right: 1px solid #e5e5e5;">
                        <div class="display-table width-100 height-100">
                            <div class="display-table-cell vertical-align-middle">
                                <!-- start tab navigation -->
                                <ul class="nav nav-tabs alt-font text-uppercase text-small display-inherit font-weight-600">
                                    <li class="active"><a href="#tab-four1" data-toggle="tab">Tribal Registration
                                        </a></li>
                                    <li><a href="#tab-four2" data-toggle="tab">Downloadable Forms</a></li>
                                    <li><a href="#tab-four3" data-toggle="tab">Frequently Asked Questions </a></li>
                                    <li><a href="#tab-four4" data-toggle="tab">Genealogy Information</a></li>
                                    <li><a href="#tab-four5" data-toggle="tab">Updating Your Information</a></li>
                                    <li><a href="#tab-four6" data-toggle="tab">Contact Us</a></li>
                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 no-padding-left">
                        <div class="tab-content" style="border: 0">
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in active" id="tab-four1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Cherokee Nation Registration Office processes Certificate of Degree of Indian Blood (CDIB), Dawes and Tribal Citizenship applications and issues CDIB, Citizenship, and Photo ID cards. The Registration office also issues Indian Preference Letters, provides verification of Tribal Citizenship, verifies eagle feather applications and provides registration services as needed. 
                                                <br><br>
                                                The basic criteria for CDIB/Cherokee Nation tribal citizenship is that an application must be submitted along with documents that directly connect a person to an enrolled lineal ancestor who is listed on the “Dawes Roll” Final Rolls of Citizens and Freedman of the Five Civilized Tribes.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four2">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Downloadable Forms
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Below are common forms used by Tribal Registration.
                                            </p>
                                            <br>
                                            <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                <p class="no-margin-bottom public-notices">
                                                    Downloads
                                                </p>

                                                <p class="no-margin-bottom downloadPDF" >
                                                    <a href="https://www.cherokee.org/media/cwbnishl/authorization-for-release-of-information-form.pdf" style="border-bottom: 1px solid blue;">
                                                        Authorization for Release of Information
                                                    </a> <br/>
                                                    <span style="font-size: 14px; font-style: italic;"> 120.2 KB -- Updated:5/31/2019</span>
                                                    <br><br>
                                                    <span>This form is used for the release of personal information.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four3">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Frequently Asked Questions
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Below you will find some answers to common questions we get asked.
                                            </p>
                                            <br/>

                                                           <!-- start accordion -->
                        <div class="panel-group accordion-style1" id="accordion-one">
                            <!-- start accordion item -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion-one" href="#accordion-one-link1" class="collapsed" aria-expanded="false"><div class="panel-title font-weight-500 text-uppercase position-relative padding-20px-right">Will your stimulus payment be affected if you owe child support?<span class="pull-right position-absolute right-0 top-0"><i class="ti-plus"></i></span></div></a>
                                </div>
                                <div id="accordion-one-link1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            The Treasury Department provides frequently asked questions on economic impact payments and the Treasury Offset Program. For more information please visit their website at https://fiscal.treasury.gov/top/faqs-for-the-public-covid-19.html
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- end accordion item -->
   
                            <!-- start accordion item -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion-one" href="#accordion-one-link4" class="collapsed" aria-expanded="false"><div class="panel-title font-weight-500 text-uppercase position-relative padding-20px-right">What is the purpose of child support?<span class="pull-right position-absolute right-0 top-0"><i class="ti-plus"></i></span></div></a>
                                </div>
                                <div id="accordion-one-link4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            Child support is based upon the concept that children are entitled to the support of both parents. This recognizes that there are costs associated with raising children. For example, the custodial parent often has to secure larger living quarters than he/she would otherwise need. The custodial parent may need larger or different transportation, in addition to basic food, clothing, school supplies, medical expenses and so forth. The custodial parent is often paying these things directly. Child support is designed to partially offset these expenses and even out the burden based upon the parent's respective incomes.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- end accordion item -->
                            <!-- start accordion item -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion-one" href="#accordion-one-link5" class="collapsed" aria-expanded="false"><div class="panel-title font-weight-500 text-uppercase position-relative padding-20px-right">Why is there a child support program?<span class="pull-right position-absolute right-0 top-0"><i class="ti-plus"></i></span></div></a>
                                </div>
                                <div id="accordion-one-link5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            CN OCSS was established in July 2007 and administers the program with in-house staff and through.
                                            <br><br>
                                            In December 1974, the U.S. Congress amended Title IV of the Social Security Act by adding Part D – Child Support and Establishment of Paternity. Title IV-D required each state to designate an organization to administer a plan for enforcing child support. Services provided by CSS to parents under this law are called IV-D cases.
                                            <br><br> 
                                            Cherokee Nation has always valued parent’s inherent right to provide for their children.   In December 2004, the Cherokee Nation tribal council recognized a need to protect Cherokee sovereignty, customs, and family values by approving a resolution that allowed for the application of funding for a IV-D Cherokee Nation child support program.   The Cherokee Nation Office of Child Support Services ( CN OCSS) was established in 2007 and administer the program with in-house staff who strives to provide the very best services to all program participants. 
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- end accordion item -->
                        </div>
                        <!-- end accordion -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four4">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Genealogy Information
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Note: The research information and links below are not maintained by the Cherokee Nation.
                                                <br><br>
                                                To search the Guion Miller and Dawes/Freedman rolls, visit:
                                                <br><br>
                                                Dawes/Freedman Roll Search – NARA Archival Information Locator (NAIL): www.archives.gov
                                                <br><br>
                                                Dawes Rolls can also be searched at Access Geneology
                                                <br><br>
                                                The Cherokee Heritage Center has a genealogist available to assist in researching Cherokee ancestry for a fee. Call 918-456-6007 visit www.cherokeeheritage.org.
                                                <br><br>
                                                Those tracing an ancestor who came from Tennessee, Georgia, eastern Alabama or the South Carolina area may want to correspond with: 
                                                <br><br>
                                                The Eastern Band of Cherokee Indians<br>
                                                Qualla Boundary<br>
                                                PO Box 455<br>
                                                Cherokee, NC 28719<br>
                                                828-497-4771<br>
                                                https://ebci.com<br>
                                                <br><br>
                                                If you need further genealogy assistance at other times, the Muskogee Public Library, 801 West Okmulgee in Muskogee, Okla., may be able to help. Call 918-682-6657. It contains most of the Cherokee Dawes applications and the Miller Roll applications, as well as additional federal census records. 
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four5">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Updating Your Information
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Have you lost your Cherokee Nation citizenship card or had a name or address change?
                                                <br><br>
                                                If so, complete the Tribal Registration Request Form available on the Downloadable Forms page. The request must contain the following with the completed Tribal Registration Form:
                                                <br><br>
                                                One clear copy of a state-issued driver’s license or state-issued identification card or Cherokee Nation citizenship (blue) card. If using a blue card, be sure it has been signed.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four-1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Talking Leaves Job Corps
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Talking Leaves Job Corps Center opened its doors on October 18, 1978. The center was formerly housed at Northeastern State University’s Loeser Hall. As the enrollment grew and the need for dorm space increased, TLJC moved to the Cherokee Nation in 1988 and later onto their current facility in 1994. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Today, TLJC enjoys a new campus currently serving 197 students and recently received a new five-year contract to continue serving students and assist them in becoming employable for the workforce and equipped for higher education.  
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                For more than four decades, Job Corps has made a difference in the lives of more than 2.6 million economically disadvantaged young Americans. Through a willingness to embrace and welcome change, this voluntary education and job training program continues to offer innovative career technical, academic and social skills training to students at 125 centers nationwide.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TLJC has open enrollment and is currently taking applications. For more information or to apply call 918-456-9959.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four7">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                TERO
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Tribal Employment Rights Office monitors and enforces the Cherokee Nation Tribal TERO Ordinance to ensure employment rights are protected.  Enforcement of employment rights is funded through the Equal Employment Opportunity Commission (E.E.O.C.)  The TERO office will investigate and mediate employment discrimination complaints involving unfair employment procedures or practices prohibited by Title VII, ADA, ADEA, & EPA.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The TERO office also negotiates for job vacancies with contractors doing business with the Cherokee Nation and refers qualified Native American workers to fill those vacancies.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TERO also maintains a list of Native-owned businesses. This list is used by CNE, CNI and the Cherokee Nation Purchasing Department when letting contracts out for bid.  The TERO Ordinance allows for TERO Certified firms to receive preferential treatment in the bid process.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Applicants wishing to apply as a TERO vendor can apply with the TERO office, where the application and all requested documentation will be reviewed. Once the application is complete a site visit will be conducted, and then the applicant will be invited to meet with the TERO committee which decides to certify or not certify a company. If certified, the vendor name will be placed on the TERO vendor list.  If denied certification, vendors many not reapply for a period of one year.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->

                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four8">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Vocational Education and Training
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training, also known as Vocational Education and Training and Career and Technical Education, provides job-specific technical training for work in the trades. These programs generally focus on providing students with hands-on instruction and can lead to certification, a diploma or certificate.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training can also give applicants an edge in job searches since they already have the certifiable knowledge they need to enter the field.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four9">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Vocational Rehabilitation Program
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    The Cherokee Nation Vocational Rehabilitation (CNVR) Program began in 1992 as the first continuous Tribal Vocational Rehabilitation program in the state of Oklahoma. It is a discretionary grant with a five-year grant cycle. Vocational Rehabilitation's fiscal year is from October 1 to September 30. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    CNVR is not a scholarship program but is based on eligibility. If a person is determined eligible, CNVR is able to assist with the cost of training/re-training at universities, colleges, and technology centers to acquire the skills and education needed to become gainfully employed or retain gainful employment.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four6">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Contact Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <div class="widget-title alt-font text-extra-small text-uppercase margin-15px-bottom font-weight-600">contact information</div>
                                                <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                                                <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                                                <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                    </div>       
                </div>                                
            </div>
        </section>
        <!-- end tab style 04 section -->

<?php
include_once('footer.php');
?>
