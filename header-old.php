<?php
    echo ' 

    <!doctype html>
<html class="no-js" lang="en">

<head>
    <!-- title -->
    <title>TSALAGI AYELI TSALAGI NATION</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
    <!-- favicon -->
    <link rel="shortcut icon" href="images/favicon.jpg">
    <!-- animation -->
    <link rel="stylesheet" href="css/animate.css" />
    <!-- bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- et line icon -->
    <link rel="stylesheet" href="css/et-line-icons.css" />
    <!-- font-awesome icon -->
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <!-- themify icon -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- swiper carousel -->
    <link rel="stylesheet" href="css/swiper.min.css">
    <!-- justified gallery  -->
    <link rel="stylesheet" href="css/justified-gallery.min.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <!-- revolution slider -->
    <link rel="stylesheet" type="text/css" href="revolution/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/css/navigation.css">
    <!-- bootsnav -->
    <link rel="stylesheet" href="css/bootsnav.css">
    <!-- style -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- responsive css -->
    <link rel="stylesheet" href="css/responsive.css" />

    <!--[if IE]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->
    <style>
        .bottom-left {
        position: absolute;
        bottom: 0px;
        background-color: #000000d4;
        color: white;
        padding: 8px;
        width: 100%;
        text-align: left;
        }
        .public-notices{
            font-size: 1.2em;
            color: #585d65;
            background-color: lightgray;
            font-size: 1.2em;
            color: #585d65;
            font-weight: 600;
            padding: 10px;
        }
        .downloadPDF{
            padding: 10px;
            font-size: 1.2em;
        }
        table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        }

        td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
        }

        tr:nth-child(even) {
        background-color: #dddddd;
        }
    </style>
</head>

    <body>
        <!-- start header -->
        <header class="header-with-topbar">
            <!-- topbar -->
            <div class="top-header-area padding-10px-tb" style="background: #99383b url("images/top-bar-bg.png") center center repeat-x; border-bottom: 2px solid #acb1bc;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 text-uppercase alt-font xs-no-padding-lr xs-text-center">
                            <a class="text-link-white xs-width-100">Newsletters</a>
                            <div class="separator-line-verticle-extra-small bg-white display-inline-block margin-two-lr hidden-xs position-relative vertical-align-middle top-minus1"></div>
                            <a class="text-link-white xs-width-100">Employment</a>
                            <div class="separator-line-verticle-extra-small bg-white display-inline-block margin-two-lr hidden-xs position-relative vertical-align-middle top-minus1"></div>
                            <a class="text-link-white xs-width-100">Contact Us</a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 hidden-xs xs-no-padding-lr text-right">
                            <div class="icon-social-medium xs-width-100 xs-text-center display-inline-block">
                                <a href="https://www.facebook.com/" title="Facebook" target="_blank" class="text-link-white"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
                                <a href="https://twitter.com/" title="Twitter" target="_blank" class="text-link-white"><i class="fab fa-twitter"></i></a>
                                <a href="https://linkedin.com/" title="Linked In" target="_blank" class="text-link-white"><i class="fab fa-linkedin-in"></i></a>
                                <a href="https://plus.google.com" title="Google Plus" target="_blank" class="text-link-white"><i class="fab fa-google-plus-g"></i></a>                            
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
            <!-- end topbar -->
            <!-- start navigation -->
            <nav class="navbar navbar-default bootsnav navbar-top header-light-transparent bg-transparent nav-box-width" style="border-bottom: 5px solid #99383b;">
                <div class="container nav-header-container">
                    <div class="row">
                        <!-- start logo -->
                        <div class="col-md-2 col-xs-5">
                            <a href="index.php" title="TSALAGI" class="logo"> 
                                <span style="font-size: 16px; font-weight: 600; color: #99383b;">
                                    Tsalagi (Cherokee) Indigenous Native American Association of Tribal
                                </span>
                            </a>
                        </div>
                        <!-- end logo -->
                        <div class="col-md-7 col-xs-2 width-auto pull-right accordion-menu xs-no-padding-right">
                            <button type="button" class="navbar-toggle collapsed pull-right" data-toggle="collapse" data-target="#navbar-collapse-toggle-1">
                                <span class="sr-only">toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="navbar-collapse collapse pull-right" id="navbar-collapse-toggle-1">
                                <ul id="accordion" class="nav navbar-nav navbar-left no-margin alt-font text-normal" data-in="fadeIn" data-out="fadeOut">
                                    <!-- start menu item -->
                                    <li class="dropdown megamenu-fw">
                                        <a href="#">All Services</a><i class="fas fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>
                                        <!-- start sub menu -->
                                        <div class="menu-back-div dropdown-menu megamenu-content mega-menu collapse mega-menu-full">
                                            <ul class="equalize sm-equalize-auto">
                                                <!-- start sub menu column  -->
                                                <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <!-- start sub menu item  -->
                                                    <ul>
                                                        <li><a href="career-services.php">COMMERCE DIVISION</a></li>
                                                        <li><a href="commerce-services.php">COMMUNITY SERVICE DIVISION</a></li>
                                                        <li><a href="community-services.php">DIVISION OF HEALTH AND HUMAN SERVICES</a></li>
                                                        <li><a href="child-care.php">DIVISION OF TRANSPORTATION</a></li>
                                                        <li><a href="child-support.php">DIVISION OF TRIBAL COMMUNICATION</a></li>
                                                        <li><a href="cultural-outreach.php">Community & Cultural Outreach</a></li>
                                                    </ul>
                                                    <!-- end sub menu item  -->
                                                </li>
                                                <!-- end sub menu column -->

                                                <!-- start sub menu column  -->
                                                <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <!-- start sub menu item  -->
                                                    <ul>
                                                    <li><a href="education-services.php">Education Services</a></li>
                                                    <li><a href="emergency-management.php">Emergency Management</a></li>
                                                    <li><a href="health-division.php">Health Division</a></li>
                                                    <li><a href="housing-authority.php">Housing Authority</a></li>
                                                    <li><a href="human-services.php">Human Services</a></li>
                                                    <li><a href="indian-child.php">Indian Child Welfare</a></li>
                                                </ul>
                                                    <!-- end sub menu item  -->
                                                </li>
                                                <!-- end sub menu column -->

                                                <!-- start sub menu column  -->
                                                <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <!-- start sub menu item  -->
                                                    <ul>
                                                        <li><a href="marshal-service.php">Marshal Service</a></li>
                                                        <li><a href="one-fire.php">One Fire</a></li>
                                                        <li><a href="real-state.php">Real Estate Services</a></li>
                                                        <li><a href="tag-office.php">Tag Office</a></li>
                                                        <li><a href="tribal-registration.php">Tribal Registration</a></li>
                                                    </ul>
                                                    <!-- end sub menu item  -->
                                                </li>
                                                <!-- end sub menu column -->

                                            </ul>
                                            <!-- end sub menu -->
                                        </div>
                                    </li>
                                    <!-- end menu item -->

                                    <!-- start menu item -->
                                    <li class="dropdown megamenu-fw">
                                        <a href="#">Our Government</a><i class="fas fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>
                                        <!-- start sub menu -->
                                        <div class="menu-back-div dropdown-menu megamenu-content mega-menu collapse mega-menu-full">
                                            <ul class="equalize sm-equalize-auto">
                                                <!-- start sub menu column  -->
                                                <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <!-- start sub menu item  -->
                                                    <ul>
                                                        <li><a href="#">Executive Branch</a></li>
                                                        <li><a href="#">Judicial Branch</a></li>
                                                        <li><a href="#">Legislative Branch</a></li>
                                                        <li><a href="#">Cherokee Nation Constitution</a></li>
                                                    </ul>
                                                    <!-- end sub menu item  -->
                                                </li>
                                                <!-- end sub menu column -->

                                                <!-- start sub menu column  -->
                                                <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <!-- start sub menu item  -->
                                                    <ul>
                                                        <li><a href="#">Office of the Attorney General</a></li>
                                                        <li><a href="#">Office of Veteran Affairs</a></li>
                                                        <li><a href="#">Secretary of Natural Resources Office</a></li>
                                                        <li><a href="#">Environmental Protection Commission</a></li>
                                                    </ul>
                                                    <!-- end sub menu item  -->
                                                </li>
                                                <!-- end sub menu column -->

                                                <!-- start sub menu column  -->
                                                <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <!-- start sub menu item  -->
                                                    <ul>
                                                        <li><a href="#">Election Commission</a></li>
                                                        <li><a href="#">Gaming Commission</a></li>
                                                        <li><a href="#">Tax Commissions</a></li>
                                                        <li><a href="#">Institutional Review Board</a></li>
                                                    </ul>
                                                    <!-- end sub menu item  -->
                                                </li>
                                                <!-- end sub menu column -->

                                            </ul>
                                            <!-- end sub menu -->
                                        </div>
                                    </li>
                                    <!-- end menu item -->

                                    <!-- start menu item -->
                                    <li class="dropdown megamenu-fw">
                                            <a href="#">Employment</a><i class="fas fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>
                                            <!-- start sub menu -->
                                            <div class="menu-back-div dropdown-menu megamenu-content mega-menu collapse mega-menu-full">
                                                <ul class="equalize sm-equalize-auto">
                                                    <!-- start sub menu column  -->
                                                    <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <!-- start sub menu item  -->
                                                        <ul>
                                                            <li><a href="#">Cherokee Elder Care</a></li>
                                                            <li><a href="#">Cherokee Nation Businesses</a></li>
                                                        </ul>
                                                        <!-- end sub menu item  -->
                                                    </li>
                                                    <!-- end sub menu column -->
    
                                                    <!-- start sub menu column  -->
                                                    <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <!-- start sub menu item  -->
                                                        <ul>
                                                            <li><a href="#">Cherokee Nation Housing Authority</a></li>
                                                            <li><a href="#">Cherokee Nation Human Resources</a></li>
                                                        </ul>
                                                        <!-- end sub menu item  -->
                                                    </li>
                                                    <!-- end sub menu column -->
    
                                                    <!-- start sub menu column  -->
                                                    <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <!-- start sub menu item  -->
                                                        <ul>
                                                            <li><a href="#">Election Commission</a></li>
                                                        </ul>
                                                        <!-- end sub menu item  -->
                                                    </li>
                                                    <!-- end sub menu column -->
    
                                                </ul>
                                                <!-- end sub menu -->
                                            </div>
                                    </li>
                                    <!-- end menu item -->

                                    <!-- start menu item -->
                                    <li class="dropdown megamenu-fw">
                                        <a href="#">About The Nation</a><i class="fas fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>
                                        <!-- start sub menu -->
                                        <div class="menu-back-div dropdown-menu megamenu-content mega-menu collapse mega-menu-full">
                                            <ul class="equalize sm-equalize-auto">
                                                <!-- start sub menu column  -->
                                                <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <!-- start sub menu item  -->
                                                    <ul>
                                                        <li><a href="#">2020 Census</a></li>
                                                        <li><a href="#">Anadisgoi News Room</a></li>
                                                        <li><a href="#">Cherokee Language</a></li>
                                                        <li><a href="#">Cherokee National Holiday</a></li>
                                                        <li><a href="#">Culture</a></li>
                                                    </ul>
                                                    <!-- end sub menu item  -->
                                                </li>
                                                <!-- end sub menu column -->

                                                <!-- start sub menu column  -->
                                                <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <!-- start sub menu item  -->
                                                    <ul>
                                                        <li><a href="#">Donations</a></li>
                                                        <li><a href="#">Financial Reports</a></li>
                                                        <li><a href="#">Frequently Asked Questions</a></li>
                                                        <li><a href="#">History</a></li>
                                                        <li><a href="#">Maps</a></li>
                                                    </ul>
                                                    <!-- end sub menu item  -->
                                                </li>
                                                <!-- end sub menu column -->

                                                <!-- start sub menu column  -->
                                                <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <!-- start sub menu item  -->
                                                    <ul>
                                                        <li><a href="#">Newsletters</a></li>
                                                        <li><a href="#">Procurements</a></li>
                                                        <li><a href="#">Public Notices</a></li>
                                                        <li><a href="#">Remember the Removal</a></li>
                                                    </ul>
                                                    <!-- end sub menu item  -->
                                                </li>
                                                <!-- end sub menu column -->

                                            </ul>
                                            <!-- end sub menu -->
                                        </div>
                                    </li>
                                    <!-- end menu item -->

                                    <!-- start menu item -->
                                    <li class="dropdown megamenu-fw">
                                        <a href="#">Visit Us</a><i class="fas fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>
                                        <!-- start sub menu -->
                                        <div class="menu-back-div dropdown-menu megamenu-content mega-menu collapse mega-menu-full">
                                            <ul class="equalize sm-equalize-auto">
                                                <!-- start sub menu column  -->
                                                <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <!-- start sub menu item  -->
                                                    <ul>
                                                        <li><a href="#">Cherokee Arts Center</a></li>
                                                        <li><a href="#">Cherokee Gift Shop</a></li>
                                                    </ul>
                                                    <!-- end sub menu item  -->
                                                </li>
                                                <!-- end sub menu column -->

                                                <!-- start sub menu column  -->
                                                <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <!-- start sub menu item  -->
                                                    <ul>
                                                        <li><a href="#">Cherokee Heritage Center</a></li>
                                                    </ul>
                                                    <!-- end sub menu item  -->
                                                </li>
                                                <!-- end sub menu column -->

                                                <!-- start sub menu column  -->
                                                <li class="mega-menu-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <!-- start sub menu item  -->
                                                    <ul>
                                                        <li><a href="#">Tourism</a></li>
                                                    </ul>
                                                    <!-- end sub menu item  -->
                                                </li>
                                                <!-- end sub menu column -->

                                            </ul>
                                            <!-- end sub menu -->
                                        </div>
                                    </li>
                                    <!-- end menu item -->
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-5 width-auto">
                            <div class="header-searchbar">
                                <a href="#search-header" class="header-search-form text-white"><i class="fas fa-search search-button"></i></a>
                                <!-- search input-->
                                <form id="search-header" method="post" action="search-result.php" name="search-header" class="mfp-hide search-form-result">
                                    <div class="search-form position-relative">
                                        <button type="submit" class="fas fa-search close-search search-button"></button>
                                        <input type="text" name="search" class="search-input" placeholder="Enter your keywords..." autocomplete="off">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- end navigation --> 
            <!-- end navigation --> 
        </header>
        <!-- end header -->
    
'; ?>