<?php
    echo ' 
    
    <!-- start footer --> 
        <footer class="footer-clean-dark bg-extra-dark-gray padding-five-tb xs-padding-30px-tb" style="background-color: #2b2d31;border-top: 8px solid #99383a;"> 
            <div class="footer-widget-area padding-30px-bottom">
                <div class="container">
                    <div class="row">
                        <!-- start logo -->
                        <div class="col-md-4 col-sm-12 col-xs-12 widget sm-margin-50px-bottom xs-margin-30px-bottom sm-text-center xs-text-left">
                            <a href="index.php" title="TSALAGI" class="display-inline-block"> 
                            <span style="font-size: 20px; font-weight: 600; color: #fff;">
                                Tsalagi (Cherokee) Indigenous Native American Association of Tribal
                            </span>
                        </a>

                        </div>
                        <!-- end logo -->
                        <!-- start contact information -->
                        <div class="col-md-4 col-sm-4 col-xs-12 widget xs-margin-30px-bottom">
                            <div class="widget-title alt-font text-extra-small text-uppercase text-white margin-15px-bottom font-weight-600">contact information</div>
                            <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                            <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                            <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                        </div>
                        <!-- end contact information -->
                        <!-- start social media -->
                        <div class="col-md-4 col-sm-4 col-xs-12 widget xs-margin-30px-bottom">
                            <div class="widget-title alt-font text-extra-small text-white text-uppercase margin-15px-bottom font-weight-600">On social networks</div>
                            <ul class="list-unstyled">
                                <li class="width-50 pull-left"><a href="https://www.facebook.com/" target="_blank" class="text-medium-gray text-small">Facebook</a></li>
                                <li class="width-50 pull-left"><a href="https://www.pinterest.com/" target="_blank" class="text-medium-gray text-small">Pinterest</a></li>
                                <li class="width-50 pull-left"><a href="https://www.twitter.com/" target="_blank" class="text-medium-gray text-small">Twitter</a></li>
                                <li class="width-50 pull-left"><a href="https://www.linkedin.com/" target="_blank" class="text-medium-gray text-small">Linkedin</a></li>
                                <li class="width-50 pull-left"><a href="https://www.dribbble.com/" target="_blank" class="text-medium-gray text-small">Dribbble</a></li>
                                <li class="width-50 pull-left"><a href="https://www.youtube.com/" target="_blank" class="text-medium-gray text-small">Youtube</a></li>
                                <li class="width-50 pull-left"><a href="https://www.instagram.com/" target="_blank" class="text-medium-gray text-small">Instagram</a></li>
                            </ul>
                        </div>
                        <!-- end social media -->
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="border-color-medium-dark-gray border-top padding-30px-top">
                    <div class="row"> 
                        <!-- start copyright -->
                        <div class="col-md-12 col-xs-12 text-right text-small text-center text-medium-gray">Copyright © 2020 TSALAGI NATION. All rights reserved.</div>
                        <!-- end copyright -->
                    </div>
                </div>
            </div>
        </footer>
        <!-- end footer -->
        <!-- start scroll to top -->
        <a class="scroll-top-arrow" href="javascript:void(0);"><i class="ti-arrow-up"></i></a>
        <!-- end scroll to top  -->
        <!-- javascript libraries -->
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/modernizr.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="js/skrollr.min.js"></script>
        <script type="text/javascript" src="js/smooth-scroll.js"></script>
        <script type="text/javascript" src="js/jquery.appear.js"></script>
        <!-- menu navigation -->
        <script type="text/javascript" src="js/bootsnav.js"></script>
        <script type="text/javascript" src="js/jquery.nav.js"></script>
        <!-- animation -->
        <script type="text/javascript" src="js/wow.min.js"></script>
        <!-- page scroll -->
        <script type="text/javascript" src="js/page-scroll.js"></script>
        <!-- swiper carousel -->
        <script type="text/javascript" src="js/swiper.min.js"></script>
        <!-- counter -->
        <script type="text/javascript" src="js/jquery.count-to.js"></script>
        <!-- parallax -->
        <script type="text/javascript" src="js/jquery.stellar.js"></script>
        <!-- magnific popup -->
        <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
        <!-- portfolio with shorting tab -->
        <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
        <!-- images loaded -->
        <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
        <!-- pull menu -->
        <script type="text/javascript" src="js/classie.js"></script>
        <script type="text/javascript" src="js/hamburger-menu.js"></script>
        <!-- counter  -->
        <script type="text/javascript" src="js/counter.js"></script>
        <!-- fit video  -->
        <script type="text/javascript" src="js/jquery.fitvids.js"></script>
        <!-- equalize -->
        <script type="text/javascript" src="js/equalize.min.js"></script>
        <!-- skill bars  -->
        <script type="text/javascript" src="js/skill.bars.jquery.js"></script> 
        <!-- justified gallery  -->
        <script type="text/javascript" src="js/justified-gallery.min.js"></script>
        <!--pie chart-->
        <script type="text/javascript" src="js/jquery.easypiechart.min.js"></script>
        <!-- instagram -->
        <script type="text/javascript" src="js/instafeed.min.js"></script>
        <!-- retina -->
        <script type="text/javascript" src="js/retina.min.js"></script>
        <!-- revolution -->
        <script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>
        <!-- revolution slider extensions (load below extensions JS files only on local file systems to make the slider work! The following part can be removed on server for on demand loading) -->
        <!--<script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>-->
        <!-- setting -->
        <script type="text/javascript" src="js/main.js"></script>
    </body>
</html>


'; ?>
