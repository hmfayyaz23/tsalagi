<?php
	include_once('header.php');
?>

        <!-- start tab style 04 section -->
        <section class="wow fadeIn padding-six-tb bg-light-gray" style="margin-top: 123px;">
            <div class="container tab-style4">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12 margin-30px-bottom xs-margin-40px-bottom">
                        <div class="position-relative overflow-hidden width-100">
                            <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Commerce Division </h5>
                        </div>
                    </div>
                </div>
                <div class="row equalize xs-equalize-auto">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding-right" style="border-right: 1px solid #e5e5e5;">
                        <div class="display-table width-100 height-100">
                            <div class="display-table-cell vertical-align-middle">
                                <!-- start tab navigation -->
                                <ul class="nav nav-tabs alt-font text-uppercase text-small display-inherit font-weight-600">
                                    <li class="active"><a href="#tab-four1" data-toggle="tab">Commerce Division</a></li>
                                    <li><a href="#tab-four2" data-toggle="tab">Adult Education</a></li>
                                    <li><a href="#tab-four3" data-toggle="tab">Employment Resources  </a></li>
                                    <li><a href="#tab-four4" data-toggle="tab">Re-entry Program</a></li>
                                    <li><a href="#tab-four5" data-toggle="tab">Surgical Technology Program</a></li>
                                    <li><a href="#tab-four6" data-toggle="tab">Talking Leaves Job Corps</a></li>
                                    <li><a href="#tab-four7" data-toggle="tab">TERO</a></li>
                                    <li><a href="#tab-four8" data-toggle="tab">Vocational Education and Training</a></li>
                                    <li><a href="#tab-four9" data-toggle="tab">Vocational Rehabilitation Program</a></li>
                                    <li><a href="#tab-four10" data-toggle="tab">Contact Us</a></li>
                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 no-padding-left">
                        <div class="tab-content" style="border: 0">
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in active" id="tab-four1">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                            Commerce Division provides tribal citizens with employment training opportunities and job assistance. The department also helps bring industry into northeastern, Oklahoma by offering recruiting fairs and employee training. Career Services also oversees many vital programs for the tribe, such as the TERO office, Summer Youth Employment Program, Job Corps, Adult Education Program, vocational training, Reentry program and more.
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                            Commerce Division offers several in-house training programs, as well as, assistance to those who are enrolled in vocational training or associate of Applied Arts or Applied Science degree programs.
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                In addition to programs for the general public, there are also programs specific to housing residents, individuals with disabilities, and youth.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four2">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Adult Education </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Adult Education Program offers High School Equivalency (GED and HISET) classes during the morning, afternoon and evening to those living in the tribe’s jurisdictional service area. The program also offers help with career and life skills development. It also provides assessments based on interests to guide individuals on the right career path.                                  
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four3">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Employment Resources
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Career Services provides resources to help individuals achieve and maintain good work habits, promote job skills
                                                 and gain employment. Through programs encompassing education, training and rehabilitation, the department helps
                                                  individuals to become successful and self-sufficient for themselves and their families.  
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four4">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Re-entry Program
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Coming Home Re-entry Program began in 2013 and was funded through a donation to help homeless incarcerated Tsalagi Nation citizens. Currently, the Tribal Council appropriates a budget to operate the program. The re-entry program serves tribal citizens who have been released from incarceration. Applications must be submitted within three months from the date of release. The program assists with employment and vocational services. Services provided are on a case-by-case basis and may include assistance with employment, training, education, housing and/or clothing.
                                            </p>

                                            <h6 class="alt-font font-weight-700 margin-15px-bottom margin-15px-top" style="color: #585d65;"> 
                                                    Eligibility for Services:
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                >  Must have been released from incarceration three months from the date of application.
                                                <br><br>
                                                >  Reside within the Tsalagi Nation’s 14-county jurisdiction in Oklahoma (Adair, Tsalagi, Craig, Nowata, Sequoyah and Washington counties, and parts of Delaware, Mayes, McIntosh, Muskogee, Ottawa, Rogers, Tulsa and Wagoner counties)
                                                <br><br>
                                                >  Must be a Tsalagi Nation citizen.

                                                <br><br><br>
                                                Required documents and other eligibility requirements will be discussed when speaking with a re-entry counselor.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four5">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Surgical Technology Program
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Surge Tech Program prepares competent entry-level surgical technologists in the cognitive, skills and behavior learning domains. A surgical technologist is an integral member of the surgical team, responsible for preparation of sterile supplies, equipment and instruments, and assisting the surgeon in their use. The surgical technologist most frequently serves as instrument handler, setting up instruments and passing them to the surgeon. A surgical technologist may also assist the surgeon by performing tasks such as retracting incisions, cutting sutures, and utilizing suction to maintain a clear field of vision for the surgeon.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Tsalagi Nation Surgical Technology Program is accredited through the Commission on Accreditation of Allied Health Education Programs (CAAHEP).  
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Accreditation exists to establish, maintain and promote appropriate standards of quality for educational programs. These standards are used for the development, evaluation, and self-analysis of the surgical technology program. Accreditation in the health-related disciplines also serves a very important public interest. Along with certification, accreditation is a tool intended to help assure a well-prepared and qualified workforce providing health care services. Students who graduate from an accredited program are eligible to take the certification examination. 
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four6">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Talking Leaves Job Corps
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Talking Leaves Job Corps Center opened its doors on October 18, 1978. The center was formerly housed at Northeastern State University’s Loeser Hall. As the enrollment grew and the need for dorm space increased, TLJC moved to the Tsalagi Nation in 1988 and later onto their current facility in 1994. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Today, TLJC enjoys a new campus currently serving 197 students and recently received a new five-year contract to continue serving students and assist them in becoming employable for the workforce and equipped for higher education.  
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                For more than four decades, Job Corps has made a difference in the lives of more than 2.6 million economically disadvantaged young Americans. Through a willingness to embrace and welcome change, this voluntary education and job training program continues to offer innovative career technical, academic and social skills training to students at 125 centers nationwide.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TLJC has open enrollment and is currently taking applications. For more information or to apply call 918-456-9959.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four7">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                TERO
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Tribal Employment Rights Office monitors and enforces the Tsalagi Nation Tribal TERO Ordinance to ensure employment rights are protected.  Enforcement of employment rights is funded through the Equal Employment Opportunity Commission (E.E.O.C.)  The TERO office will investigate and mediate employment discrimination complaints involving unfair employment procedures or practices prohibited by Title VII, ADA, ADEA, & EPA.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The TERO office also negotiates for job vacancies with contractors doing business with the Tsalagi Nation and refers qualified Native American workers to fill those vacancies.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TERO also maintains a list of Native-owned businesses. This list is used by CNE, CNI and the Tsalagi Nation Purchasing Department when letting contracts out for bid.  The TERO Ordinance allows for TERO Certified firms to receive preferential treatment in the bid process.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Applicants wishing to apply as a TERO vendor can apply with the TERO office, where the application and all requested documentation will be reviewed. Once the application is complete a site visit will be conducted, and then the applicant will be invited to meet with the TERO committee which decides to certify or not certify a company. If certified, the vendor name will be placed on the TERO vendor list.  If denied certification, vendors many not reapply for a period of one year.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->

                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four8">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Vocational Education and Training
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training, also known as Vocational Education and Training and Career and Technical Education, provides job-specific technical training for work in the trades. These programs generally focus on providing students with hands-on instruction and can lead to certification, a diploma or certificate.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training can also give applicants an edge in job searches since they already have the certifiable knowledge they need to enter the field.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four9">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Vocational Rehabilitation Program
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    The Tsalagi Nation Vocational Rehabilitation (CNVR) Program began in 1992 as the first continuous Tribal Vocational Rehabilitation program in the state of Oklahoma. It is a discretionary grant with a five-year grant cycle. Vocational Rehabilitation's fiscal year is from October 1 to September 30. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    CNVR is not a scholarship program but is based on eligibility. If a person is determined eligible, CNVR is able to assist with the cost of training/re-training at universities, colleges, and technology centers to acquire the skills and education needed to become gainfully employed or retain gainful employment.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four10">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Contact Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <div class="widget-title alt-font text-extra-small text-uppercase margin-15px-bottom font-weight-600">contact information</div>
                                                <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                                                <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                                                <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                    </div>       
                </div>                                
            </div>
        </section>
        <!-- end tab style 04 section -->

<?php
include_once('footer.php');
?>