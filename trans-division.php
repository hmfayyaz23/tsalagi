<?php
	include_once('header.php');
?>

        <!-- start tab style 04 section -->
        <section class="wow fadeIn padding-six-tb bg-light-gray" style="margin-top: 123px;">
            <div class="container tab-style4">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12 margin-30px-bottom xs-margin-40px-bottom">
                        <div class="position-relative overflow-hidden width-100">
                            <h5 class="alt-font font-weight-700 margin-15px-bottom" style="color: #99383b;"> Division of Transportation </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding-right" style="border-right: 1px solid #e5e5e5;">
                        <div class="display-table width-100 height-100">
                            <div class="display-table-cell vertical-align-middle">
                                <!-- start tab navigation -->
                                <ul class="nav nav-tabs alt-font text-uppercase text-small display-inherit font-weight-600">
                                    <li class="active"><a href="#tab-four1" data-toggle="tab">Division of Transportation </a></li>
                                    <li><a href="#tab-four2" data-toggle="tab">Licensing</a></li>
                                    <li><a href="#tab-four3" data-toggle="tab">Resource and Referral </a></li>
                                    <li><a href="#tab-four4" data-toggle="tab">Subsidy</a></li> 
                                </ul>
                                <!-- end tab navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 no-padding-left">
                        <div class="tab-content" style="border: 0">
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in active" id="tab-four1">
                                <div class="row ">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                            Division of Transportation  operates two child care facilities and several programs including: Resource and Referral, Licensing, and Subsidy. The goal of child care and development is to increase the availability, affordability, and quality of childcare and provide training opportunities for parents.
                                                <br><br>
                                                Resource and Referral offer child care providers technical assistance and training. Parents can obtain referrals to quality child care options in their area. Tsalagi PARENTS offers parenting classes and assistance for parents.
                                                <br><br>
                                                Licensing provides monitoring visits to over 500 licensed and registered child care providers to ensure compliance with OKDHS Child Care Licensing standards.
                                                <br><br>
                                                Subsidy provides financial assistance with child care payments to over 2,500 children annually. Parents of children birth-12 years of age who are working, looking for work, going to school, attending job-training programs or are in need of protective services receive this service.
                                                <br><br>
                                                Two-Tribally owned and operated centers are located in Tahlequah and Stilwell. The centers are licensed to care for up to 200 children ages 6 weeks to 4 years.
                                                <br><br>
                                                For more information about Child Care and Development Resource and Referral, Licensing and Subsidy please contact:
                                                <br><br>
                                                Resource and Referral - ccrc@Tsalagi.org
                                                Licensing - childcarelicensing@Tsalagi.org
                                                Subsidy – cdc_resource@Tsalagi.org
                                                <br><br>
                                                Or call 918-453-5300 or 888-458-6230
                                                <br><br>
                                                For more information about enrollment in a center, please contact:
                                                <br><br>
                                                Tahlequah Child Development Center – 918-453-5070
                                                Stilwell Child Development Center – 918-696-3222
                                            </p>
                                            <br>
                                            
                                            <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                <p class="no-margin-bottom public-notices">
                                                    Public Notices
                                                </p>
                                                <p class="no-margin-bottom downloadPDF" >
                                                    <a href="https://childcare.Tsalagi.org/media/4q5dm1kv/updated-5-22-20-covid-19-Tsalagi-nation-subsidy-policy-for-parents-and-providers.pdf" style="border-bottom: 1px solid blue;">
                                                        Child Care & Development COVID-19 Emergency Policy
                                                    </a> <br/>
                                                    <span style="font-size: 14px; font-style: italic;"> 604.4 KB -- Created:3/17/2020  |  Updated:5/26/2020</span>
                                                    <br><br>
                                                    <span>Emergency Policy for childcare providers and subsidy regarding COVID-19.</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four2">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> Licensing
                                            </h6>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;"> Requirements for Contract Providers                                            </p>
                                            <br>
                                           <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                            To become a contracted child care provider for the Tsalagi Nation, a provider must submit an application by mail (OKDHS Licensed Providers) or in person to a Child Care Worker in one of the offices. On-site inspections will be made. Periodic monitoring visits will be made to insure continued quality of services and compliance with Health and Safety Requirements.
                                            <br><br>
                                            Licensed Child Care Centers and Child Care Homes must be currently licensed by the Oklahoma Department of Human Services. Child Care Centers must meet a minimum of a 1+Star Rating, Tsalagi Nation requires the provider to complete an application with the following documents:
                                            <br><br>
                                            > Completed application and a signed agreement <br>
                                            > IRS W-9 form <br>
                                            > Declaration of prior criminal convictions OKDHS Child Care <br>
                                            > License <br>
                                            > Star Certificate <br>
                                            > CDIB Card (if applicable) <br>
                                            > Must be in substantial compliance with OKDHS Health and Safety <br>
                                            > Requirements IRS FEIN letter or Social Security card <br><br>

                                            Registered Family Child Care Homes known as Exempt Relative Providers are the grandparents, great grandparents, aunts, uncles, or adult sibling not living in the home of the child. The relationship can be by blood or marriage. Relative Providers are exempt from DHS licensing. Registered providers must meet the tribes Health and Safety Standards of Operation and Quality of Service. Periodic monitoring visits are made. Documents include:
                                            <br><br>
                                            > Completed application and signed agreement <br>
                                            > Form IRS W-9 form as an independent contractor <br>
                                            > Declaration of Prior Criminal Convictions Authorization for OSBI Background Check <br>
                                            > Proof of Residence <br>
                                            > CDIB Card (if applicable) <br>
                                            > Driver’s License and Social Security Card <br>
                                            > Address Verification <br> <br>

                                            <b>“Reaching for the Stars” Program</b> <br> <br>
                                            
                                            Reaching for the Stars was developed to help parents identify quality child care programs more easily. The Department of Human Services requires that licensed child care programs meet additional quality criteria in order to be rated with two-or-three stars.
                                            Click here (http://www.okdhs.org/services/cc/Pages/childcareSTARS.aspx) for more information about the Stars Rating System.
                                            <br> <br>
                                            <b>Provider Assistance</b>
                                            <br> <br>
                                            Do you need help or information on the Stars system, accreditation, licensing information, or have questions about child care? We can help you find the answer. We have staff who are knowledgeable on child care, and have access to the internet and lots of informational files. We also offer technical assistance by telephone or on-site to help assist you with questions relating from room arrangement to implementing lesson plans.
                                            <br> <br>
                                            <b>Helpful links for Providers</b>
                                            <br> <br>
                                            > The Activity Idea Place  <br>
                                            > Association for Library Service to Children (A Division of the American Library Association)  <br>
                                            > Center of Early Childhood Professional Development  <br>
                                            > Child Care Aware of America  <br>
                                            > National Association for the Education of the Young Children  <br>
                                            > National Association for Family Child Care  <br>
                                           </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four3">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Resource and Referral
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65; font-weight: 600">
                                                Child Care Resource Center 
                                            </p>
                                            <br>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Child Resource Center is a community service organization that works with parents, child care providers, businesses, and community organizations to help promote the availability of quality child care services in the area. It provides parents with child care referrals and information on evaluating quality child care, plus resources on various parenting issues. Child care referrals are provided free-of-charge and information collected from families will remain confidential. The Child Care Resource Center offers valuable training and support services to new or established child care programs.                                            </p>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four4">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Subsidy
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Parents and guardians applying for subsidy for the first time must complete their application in person.  Clients recertifying may submit documents by mail, email or fax to their assigned caseworker. There are 7 subsidy office locations including the Tahlequah office. 
                                                <br><br>
                                                Eligibility Guidelines:
                                                <br><br>
                                                1. Parents/Guardians must be working, going to school, in need of protective services and/or actively participating in a structured job search for the purpose of obtaining employment. Includes: high school, college, post-graduate work, GED preparation, Vo-tech, and job skills training programs
                                                <br>
                                                2. Household income must be at or below the amount listed for your family size to receive assistance paying child care expenses. See income guidelines in the downloads section below.
                                                <br>
                                                3. Parent and/or child has a CDIB card or proof of tribal citizenship.
                                                <br>
                                                4. Family lives within the Tsalagi Nation Child Care & Development Service Area. The Tsalagi Nation Child Care & Development Service Area consists of 14 counties in extreme northeastern Oklahoma.
                                                <br>
                                                5. Children may not be older than 12 years of age. However, children with special needs may qualify for services up to age 19. A child with special needs is any child who has a documented medical disability that requires special care.
                                                <br>
                                                6. The parent/guardian must use a child care facility or relative provider contracted with the Tsalagi Nation.
                                                <br>
                                                7. The parent/guardian will need to bring the documents listed on the Required Document List.
                                                <br><br>
                                                <b>How to Apply for Subsidy:</b>
                                                <br><br>
                                                The initial application for child care subsidy must be made in person. We have several Subsidy office locations including the Tahlequah office. Please call a caseworker near you to schedule your appointment or prior to traveling to ensure caseworkers are available to meet with you.
                                                <br><br>
                                                Clients recertifying may submit documents by mail, email or fax to their assigned caseworker.
                                                <br><br>
                                                If the parent/guardian subsidy application is approved, certification lasts for 12 months.
                                                <br><br>
                                                <b>Documents needed to apply for child care subsidy:</b>
                                                <br><br>
                                                Parent/guardians must complete the Required Document List for your child care subsidy application to be approved. Documents on this list include:o CDIB
                                                <br><br>
                                                > birth certificate <br>
                                                > proof of residence <br>
                                                > income verification <br>
                                                > school schedule (if applicable) <br>
                                                > custody (if applicable) <br>
                                                > Client Responsibility Form <br>
                                                > Employment Verification Form <br><br>

                                                <div class="no-margin-bottom" style="border: 1px solid lightgray;">
                                                    <p class="no-margin-bottom public-notices">
                                                        Downloads
                                                    </p>
                                                    <p class="no-margin-bottom downloadPDF" >
                                                        <a href="https://childcare.Tsalagi.org/media/n2fdk24i/2020-clientresponsibilityandagreement-docx-revised-1-2-20.pdf" style="border-bottom: 1px solid blue;">
                                                            Client Responsibility Agreement
                                                        </a> <br/>
                                                        <span style="font-size: 14px; font-style: italic;"> 342.2 KB -- Updated:1/23/2020</span>
                                                        <br><br>
                                                        <span>Client responsibility agreement for clients who are participating in the Child Care Subsidy program.</span>
                                                    </p>
                                                </div>
                                            </p>

                                           <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four5">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Surgical Technology Program
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Surge Tech Program prepares competent entry-level surgical technologists in the cognitive, skills and behavior learning domains. A surgical technologist is an integral member of the surgical team, responsible for preparation of sterile supplies, equipment and instruments, and assisting the surgeon in their use. The surgical technologist most frequently serves as instrument handler, setting up instruments and passing them to the surgeon. A surgical technologist may also assist the surgeon by performing tasks such as retracting incisions, cutting sutures, and utilizing suction to maintain a clear field of vision for the surgeon.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Tsalagi Nation Surgical Technology Program is accredited through the Commission on Accreditation of Allied Health Education Programs (CAAHEP).  
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Accreditation exists to establish, maintain and promote appropriate standards of quality for educational programs. These standards are used for the development, evaluation, and self-analysis of the surgical technology program. Accreditation in the health-related disciplines also serves a very important public interest. Along with certification, accreditation is a tool intended to help assure a well-prepared and qualified workforce providing health care services. Students who graduate from an accredited program are eligible to take the certification examination. 
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four6">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Talking Leaves Job Corps
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Talking Leaves Job Corps Center opened its doors on October 18, 1978. The center was formerly housed at Northeastern State University’s Loeser Hall. As the enrollment grew and the need for dorm space increased, TLJC moved to the Tsalagi Nation in 1988 and later onto their current facility in 1994. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Today, TLJC enjoys a new campus currently serving 197 students and recently received a new five-year contract to continue serving students and assist them in becoming employable for the workforce and equipped for higher education.  
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                For more than four decades, Job Corps has made a difference in the lives of more than 2.6 million economically disadvantaged young Americans. Through a willingness to embrace and welcome change, this voluntary education and job training program continues to offer innovative career technical, academic and social skills training to students at 125 centers nationwide.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TLJC has open enrollment and is currently taking applications. For more information or to apply call 918-456-9959.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four7">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                TERO
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The Tribal Employment Rights Office monitors and enforces the Tsalagi Nation Tribal TERO Ordinance to ensure employment rights are protected.  Enforcement of employment rights is funded through the Equal Employment Opportunity Commission (E.E.O.C.)  The TERO office will investigate and mediate employment discrimination complaints involving unfair employment procedures or practices prohibited by Title VII, ADA, ADEA, & EPA.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                The TERO office also negotiates for job vacancies with contractors doing business with the Tsalagi Nation and refers qualified Native American workers to fill those vacancies.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                TERO also maintains a list of Native-owned businesses. This list is used by CNE, CNI and the Tsalagi Nation Purchasing Department when letting contracts out for bid.  The TERO Ordinance allows for TERO Certified firms to receive preferential treatment in the bid process.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Applicants wishing to apply as a TERO vendor can apply with the TERO office, where the application and all requested documentation will be reviewed. Once the application is complete a site visit will be conducted, and then the applicant will be invited to meet with the TERO committee which decides to certify or not certify a company. If certified, the vendor name will be placed on the TERO vendor list.  If denied certification, vendors many not reapply for a period of one year.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->

                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four8">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Vocational Education and Training
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training, also known as Vocational Education and Training and Career and Technical Education, provides job-specific technical training for work in the trades. These programs generally focus on providing students with hands-on instruction and can lead to certification, a diploma or certificate.
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                Vocational training can also give applicants an edge in job searches since they already have the certifiable knowledge they need to enter the field.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four9">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                    Vocational Rehabilitation Program
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    The Tsalagi Nation Vocational Rehabilitation (CNVR) Program began in 1992 as the first continuous Tribal Vocational Rehabilitation program in the state of Oklahoma. It is a discretionary grant with a five-year grant cycle. Vocational Rehabilitation's fiscal year is from October 1 to September 30. 
                                            </p>
                                            <br/>
                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                    CNVR is not a scholarship program but is based on eligibility. If a person is determined eligible, CNVR is able to assist with the cost of training/re-training at universities, colleges, and technology centers to acquire the skills and education needed to become gainfully employed or retain gainful employment.
                                            </p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in" id="tab-four10">
                                <div class="row equalize xs-equalize-auto">
                                    <div class="col-md-12 col-sm-12 col-xs-12 display-table xs-margin-30px-bottom">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h6 class="alt-font font-weight-700 margin-15px-bottom" style="color: #585d65;"> 
                                                Contact Us
                                            </h6>

                                            <p class="no-margin-bottom" style="font-size: 1.2em;color: #585d65;">
                                                <div class="widget-title alt-font text-extra-small text-uppercase margin-15px-bottom font-weight-600">contact information</div>
                                                <div class="text-small line-height-24 width-75 text-medium-gray xs-width-100">301 The Greenhouse, Custard Factory, London, E2 8DY.</div>
                                                <div class="text-small line-height-24 text-medium-gray">Email: <a href="mailto:sales@domain.com" class="text-medium-gray">sales@domain.com</a></div>
                                                <div class="text-small line-height-24 text-medium-gray">Phone: +44 (0) 123 456 7890</div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                    </div>       
                </div>                                
            </div>
        </section>
        <!-- end tab style 04 section -->

<?php
include_once('footer.php');
?>
